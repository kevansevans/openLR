package global;

import openfl.net.SharedObject;
import openfl.utils.Object;

/**
 * ...
 * @author Kaelan Evans
 * 
 * Client Variables. Variables that are controlled by player input in some form.
 */
class CVar 
{
	public static var local = { //Settings that are specific to the player. Saves to appdata
		auto_save : true,
		auto_save_freq : 10,
		angle_snap : false,
		angle_snap_value : 15.0,
		angle_snap_offset : 0.0,
		
		cloud_info : [],
		
		dictionary : "English",
		
		line_snap : true,
		
		scroll_cursor : true,
		
		welcome : true,
	};
	public static var track = { //settings that are specific to the track. Saves to track
		author : "Made by: Anonymous",
		
		description : "This is the water. And this is the well. Drink full and descend. The horse is the white of the eyes and dark within.",
		
		force_zoom : false,
		force_zoom_ammount : 2.0,
		
		last_load: "_key:NewTrack",
		
		name: "Untitled",
		
		rider_focus : 0,
	};
	public static var volatile = { //settings that are specific to the session. Does not save
		color_play : false,
		contact_points : false,
		
		flagged : false,
		
		hit_test : false,
		hit_test_live : false,
		
		paused : false,
		preview_mode : false,
		
		slow_motion_auto : false,
		slow_motion_rate : 5,
	};
	
	public static var paused:Bool = false;

	public static var mod_shift:Bool = false;
	public static var mod_ctrl:Bool = false;
	public static var mod_alt:Bool = false;
	public static var mod_x:Bool = false;
	public static var mod_z:Bool = false;
	
	public function new() 
	{
		
	}
	public static function flushLocal() {
		#if !flash
			var sol:SharedObject = SharedObject.getLocal("OLR_Local", "/olr/");
		#else
			var sol:SharedObject = SharedObject.getLocal("OLR_Local", "/");
		#end
		sol.data.local = CVar.local;
		sol.flush();
	}
	public static function loadLocal() {
		#if !flash
			var sol:SharedObject = SharedObject.getLocal("OLR_Local", "/olr/");
		#else
			var sol:SharedObject = SharedObject.getLocal("OLR_Local", "/");
		#end
		if (sol.data.local == null) return;
		CVar.local = sol.data.local;
	}
}