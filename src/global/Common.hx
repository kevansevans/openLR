package global;

import lr.menus.FileMenu;
import openfl.utils.Object;
import openfl.geom.Point;
import openfl.display.Sprite;
import platform.file.ImportBase;

import global.engine.RiderManager;
import global.engine.SimManager;
import lr.scene.Track;
import lr.scene.timeline.TimelineControl;
import lr.scene.TextInfo;
import lr.tool.Toolbar;
import lr.tool.ToolBase;
import lr.nodes.Grid;
import lr.rider.RiderCamera;

/**
 * ...
 * @author Kaelan Evans
 * 
 * This is a shortcut class. Not a variable holder. If it has to hold some form of value, it's better suited for CVar or SVar.
 * 
 */
@:enum abstract Icon(String) from String {
	public var undefined:String = "undefined";
	public var pencil:String = "pencil";
	public var line:String = "line";
	public var eraser:String = "eraser";
	public var extra:String = "extra";
	public var pan:String = "pan";
	public var zoom:String = "zoom";
	public var play:String = "play";
	public var pause:String = "pause";
	public var stop:String = "stop";
	public var flag:String = "flag";
	public var file:String = "file";
	public var settings:String = "settings";
	public var swBlue:String = "blue";
	public var swRed:String = "red";
	public var swGreen:String = "green";
	public var no:String = "no";
	public var yes:String = "yes";
	public var generic:String = "generic";
	public var info:String = "info";
	public var refresh:String = "refresh";
	public var returnB:String = "return";
	public var add:String = "add";
	public var remove:String = "remove";
	public var newRider:String = "newRider";
	public var trash:String = "trash";
	public var filebox:String = "filebox";
}
class Common
{

	public function new() 
	{
		
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//variables
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static var version:String = "0.0.6";
	
	public static var gCode:Main;
	public static var gTrack:Track;
	public static var gRiderManager:RiderManager;
	public static var gVisContainer:Sprite;
	public static var gToolBase:ToolBase;
	public static var gToolCurrent:ToolBase;
	public static var gGrid:Grid;
	public static var gSimManager:SimManager;
	public static var gTextInfo:TextInfo;
	public static var gToolbar:Toolbar;
	public static var gCamera:RiderCamera;
	public static var gTimeline:TimelineControl;
	public static var gImport:ImportBase;
	public static var gLoadMenu:FileMenu;
	
	public static var stage_tl:Point = new Point(0, 0);
	public static var stage_br:Point = new Point(0, 0);
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//functions
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static function get_angle_radians(_a:Point, _b:Point):Float {
		var _locAngle = Math.atan2(_b.y - _a.y, _b.x - _a.x);
		return (_locAngle);
	}
	public static function get_angle_degrees(_a:Point, _b:Point):Float {
		var _locAngle = Math.atan2(_b.y - _a.y, _b.x - _a.x) * 180 / Math.PI;
		return (_locAngle);
	}
	public static function get_point_vector(_dis:Int, _ang:Float):Point {
		var _loc1:Point = new Point((_dis * -1) * Math.cos(_ang), (_dis * -1) * Math.sin(_ang));
		return(_loc1);
	}
	public static function get_distance(_a:Point, _b:Point):Float {
		return(Math.sqrt(Math.pow(_b.y - _a.y, 2) + Math.pow(_b.x - _a.x, 2)));
	}
	public static function get_distance_point(_a:Point, _b:Point):Point {
		var _loc1:Point = new Point(_b.y - _a.y, _b.x - _a.x);
		return(_loc1);
	}
	public static function gridPos(x:Float, y:Float):Object {
		var posObject:Object = new Object();
		posObject.x = Math.floor(x / SVar.node_gridsize);
		posObject.y = Math.floor(y / SVar.node_gridsize);
		posObject.gx = x - SVar.node_gridsize * posObject.x;
		posObject.gy = y - SVar.node_gridsize * posObject.y;
		return(posObject);
	}
	public static function tilePos(x:Float, y:Float):Object {
		var posObject:Object = new Object();
		posObject.x = Math.floor(x / SVar.node_tilesize);
		posObject.y = Math.floor(y / SVar.node_tilesize);
		posObject.gx = x - SVar.node_tilesize * posObject.x;
		posObject.gy = y - SVar.node_tilesize * posObject.y;
		return(posObject);
	}
	
	public static function timeStamp(_frames:Int):String {
		var negative:Bool = false;
		var frames = _frames;
		if (_frames < 0) {
			negative = true;
			frames *= -1;
		}
		var seconds:Int = Std.int(frames / 40);
		var minutes:Int = Std.int(seconds / 60);
		var hours:Int = Std.int(minutes / 60);
		var remFrames:Int = frames % 40;
		var remSeconds:Int = seconds % 60;
		var remMinutes:Int = minutes % 60;
		var _returnString:String = "";
		if (frames < 40)
		{
			_returnString = (frames < 10) ? "0:0" + frames : "0:" + frames;
		}
		else if (frames >= 40 && seconds < 60)
		{
			_returnString = (remFrames < 10) ? seconds + ":" + "0" + remFrames : seconds + ":" + remFrames;
		}
		else if (seconds >= 60 && minutes < 60)
		{
			_returnString = minutes + ":" + ((remSeconds < 10) ? "0" + remSeconds : '$remSeconds') + ":" + ((remFrames < 10) ? "0" + remFrames : '$remFrames');
		} else if (minutes >= 60) {
			_returnString = hours + ":" + ((minutes < 10) ? "0" + remMinutes : '$remMinutes') + ":" + ((remSeconds < 10) ? "0" + remSeconds : '$remSeconds') + ":" + ((remFrames < 10) ? "0" + remFrames : '$remFrames');
		}
		if (negative) {
			_returnString = "-" + _returnString;
		}
		return (_returnString);
	}
	public static function globalPlay() {
		if (!CVar.paused) {
			SVar.track_last_pos_x = Common.gTrack.x;
			SVar.track_last_pos_y = Common.gTrack.y;
		}
		Common.gTrack.set_rendermode_play();
		Common.gTrack.set_simmode_play();
		
		Toolbar.tool.set_tool("None");
	}
	public static function globalStop() {
		Common.gTrack.set_rendermode_edit();
		Common.gTrack.set_simmode_stop();
		Common.gTimeline.update();
		if (!CVar.paused) {
			Common.gTrack.x = SVar.track_last_pos_x;
			Common.gTrack.y = SVar.track_last_pos_y;
		}
	}
	public static function rgb_to_hex(_r:Int, _g:Int, _b:Int):Int {
		return (_r << 16 | _g << 8 | _b);
	}
	public static function randomRange(minNum:Int, maxNum:Int):Int  
	{ 
		return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum); 
	} 
}