package global.engine;

import lr.rider.RiderBase;
import lr.rider.RiderRecorder;

/**
 * ...
 * @author Kaelan Evans
 */
class RiderManager
{
	public var riderArray:Array<RiderBase>;
	public static var crash:Array<Null<Bool>>;
	public static var speed:Array<Float>;
	public var recorder:RiderRecorder;
	
	public function new() 
	{
		Common.gRiderManager = this;
		
		this.riderArray = new Array();
		RiderManager.crash = new Array();
		RiderManager.speed = new Array();
		this.recorder = new RiderRecorder();
	}
	public function add_rider(_type:Int, _x:Float, _y:Float) {
		this.riderArray[SVar.rider_count] = new RiderBase(_type, _x, _y, SVar.rider_count);
		this.riderArray[SVar.rider_count].set_init_start();
		this.riderArray[SVar.rider_count].spawn = SVar.frames == 0 ? null : SVar.frames;
		this.riderArray[SVar.rider_count].start_point.update();
		this.riderArray[SVar.rider_count].clips.render();
		Common.gTrack.rider.addChild(this.riderArray[SVar.rider_count]);
		RiderManager.crash[SVar.rider_count] = false;
		SVar.rider_count += 1;
	}
	public function remove_rider(_rider:RiderBase) {
		this.riderArray.remove(_rider);
		Common.gTrack.rider.removeChild(_rider);
		RiderManager.crash[_rider.riderID] = null;
	}
	public function rider_wipe() {
		for (a in riderArray) {
			if (a == null) continue;
			this.remove_rider(a);
		}
		this.riderArray = new Array();
		RiderManager.crash = new Array();
		RiderManager.speed = new Array();
		SVar.rider_count = 0;
		this.add_rider(2, 0, 0);
	}
	public function set_rider_visual_start() {
		for (a in riderArray) {
			if (a == null) continue;
			a.set_rider_play_mode();
		}
	}
	public function set_rider_visual_stop() {
		for (a in riderArray) {
			if (a == null) continue;
			a.set_rider_edit_mode();
		}
	}
	public function advance_riders() {
		for (a in riderArray) {
			if (a == null) continue;
			if (a.spawn == null && a.despawn == null) {
				a.step_rider();
			} else if (SVar.frames >= a.spawn && a.despawn == null) {
				a.step_rider();
			} else if (SVar.frames >= a.spawn && SVar.frames <= a.despawn) {
				a.step_rider();
			} else if (a.spawn == null && SVar.frames <= a.despawn) {
				a.step_rider();
			}
			this.recorder.index_frame(SVar.frames, a);
		}
	}
	public function sub_step_riders() {
		for (a in riderArray) {
			if (a == null) continue;
			a.step_rider_sub();
		}
	}
	public function rewind_riders() {
		--SVar.frames;
		for (a in riderArray) {
			if (a == null) continue;
			this.recorder.inject_frame(SVar.frames, a);
		}
	}
	public function restore_start() {
		for (a in riderArray) {
			if (a == null) continue;
			this.recorder.inject_frame(0, a);
			a.reset();
			this.recorder.index_frame(0, a);
		}
	}
	public function set_flag(_renew:Bool = false) {
		if (SVar.sim_running) {
			CVar.volatile.flagged = true;
			SVar.flagged_frame = SVar.frames;
			for (a in this.riderArray) {
				if (a == null) continue;
				a.update_checkpoint(true, true);
			}
		} else {
			if (_renew) {
				CVar.volatile.flagged = true;
				SVar.flagged_frame = SVar.frames;
				for (a in this.riderArray) {
					if (a == null) continue;
					a.update_checkpoint(true, true);
				}
			} else {
				for (a in this.riderArray) {
					if (a == null) continue;
					CVar.volatile.flagged = (CVar.volatile.flagged == true) ? false : true;
					a.update_checkpoint(CVar.volatile.flagged, false);
				}
			}
		}
	}
	public function restore_flag() {
		for (a in riderArray) {
			if (a == null) continue;
			this.recorder.inject_frame(SVar.flagged_frame, a);
		}
	}
	public function update_riders(_frame:Int) {
		for (a in riderArray) {
			if (a == null) continue;
		}
	}
	public function iterate_riders_loop(_v:Int) {
		for (a in riderArray) {
			if (a == null) continue;
			a.step_rider_loop(_v);
		}
	}
	public function set_rider_colors(_index:Int, _hexA:Int, _hexB:Int) {
		this.riderArray[_index].update_color(_hexA, _hexB);
	}
	public function set_rider_spawn(_index:Int, _v:Null<Int>) {
		if (_v == -1) _v = null;
		this.riderArray[_index].spawn = _v;
	}
	public function set_rider_despawn(_index:Int, _v:Null<Int>) {
		if (_v == -1) _v = null;
		this.riderArray[_index].despawn = _v;
	}
	public function set_rider_name(_index:Int, _name:String) {
		this.riderArray[_index].update_name(_name);
	}
	public function set_rider_alpha(_index:Int, _value:Float) {
		this.riderArray[_index].rider_alpha = _value;
		this.riderArray[_index].clips.render();
	}
	public function set_rider_angle(_index:Int, _angle:Float) {
		this.riderArray[_index].set_start_angle(0);
		this.riderArray[_index].reset();
		this.riderArray[_index].set_start_angle(_angle);
		this.riderArray[_index].clips.render();
	}
	public function set_rider_skeleton(_index:Int, _v:Bool) {
		this.riderArray[_index].bone_vis = _v;
		this.riderArray[_index].clips.render();
	}
	public function set_rider_velocity(_index:Int, _v:Bool) {
		this.riderArray[_index].vel_vis = _v;
		this.riderArray[_index].clips.render();
	}
	public function set_rider_scarf(_index:Int, _v:Bool) {
		this.riderArray[_index].scarf_vis = _v;
		this.riderArray[_index].clips.render();
	}
	public function set_rider_starts() {
		for (a in riderArray) {
			if (a == null) continue;
			a.set_start(a.rider_pos_x, a.rider_pos_y);
		}
	}
	public function set_new_start(_index:Int, _xpos:Float, _ypos:Float) {
		this.riderArray[_index].set_start(_xpos, _ypos);
	}
	public function destroy_flag() {
		for (a in this.riderArray) {
			if (a == null) continue;
			a.update_checkpoint(false);
		}
	}
	public function render() {
		for (a in riderArray) {
			if (a == null) continue;
			a.clips.render();
		}
	}
	public function set_start(_x:Float, _y:Float, _id:Int = 0) {
		this.riderArray[_id].set_start(_x, _y);
		this.recorder.index_frame(0, riderArray[_id]);
	}
	public function inject_frame(_frame:Int) {
		for (a in riderArray) {
			if (a == null) continue;
			this.recorder.inject_frame(_frame, a);
			a.clips.render();
		}
	}
}