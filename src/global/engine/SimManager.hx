package global.engine;

import haxe.Timer;

import global.Common;
import global.CVar;
import lr.rider.RiderCamera;


/**
 * ...
 * @author ...
 */
class SimManager 
{
	var iterator:Timer;
	
	private var flag_av:Bool = false;
	
	private var playback_rate:Float = 40;
	private var slow_rate:Float = 5;
	private var rateBelowOneFPS:Bool = false;
	private var rewind:Bool = false;
	private var ff_loop:Float = 1;
	
	private var playbackRates:Array<Int> = [1, 2, 5, 10, 15, 20, 25, 30, 35, 40, 80, 160, 320];
	private var playbackRateIndex:Int = 9;
	
	private var camera:RiderCamera;
	
	public function new() 
	{
		Common.gSimManager = this;
		
		this.camera = new RiderCamera();
	}
	public function init_sim() {
		CVar.paused = false;
		
		SVar.track_last_pos_x = Common.gTrack.x;
		SVar.track_last_pos_y = Common.gTrack.y;
		
		Common.gTrack.set_rendermode_play();
		Common.gTrack.set_simmode_play();
	}
	public function close_sim() {
		Common.gTrack.set_rendermode_edit();
		Common.gTrack.set_simmode_stop();
		if (!CVar.volatile.flagged) {
			SVar.frames = 0;
			Common.gRiderManager.restore_start();
		} else if (CVar.volatile.flagged) {
			SVar.frames = SVar.flagged_frame;
			Common.gRiderManager.restore_flag();
		}
		Common.gTimeline.update();
		if (!CVar.paused) {
			Common.gTrack.x = SVar.track_last_pos_x;
			Common.gTrack.y = SVar.track_last_pos_y;
		}
	}
	public function start_sim(_fromStart:Bool) {
		if (CVar.volatile.slow_motion_auto) {
			SVar.slow_motion = true;
			this.playback_rate = CVar.volatile.slow_motion_rate;
			this.slow_rate = CVar.volatile.slow_motion_rate;
		} else {
			SVar.slow_motion = false;
			this.playback_rate = 40;
			this.playbackRateIndex = 9;
			SVar.playbackModifierString = "";
		}
		if (!CVar.volatile.flagged || _fromStart) {
			SVar.frames = 0;
			Common.gRiderManager.restore_start();
		} else if (CVar.volatile.flagged) {
			Common.gRiderManager.restore_flag();
		}
		if (CVar.paused) {
			CVar.volatile.paused = false;
			SVar.pause_frame = null;
		}
		Common.gRiderManager.set_rider_visual_start();
		if (!SVar.sim_running) {
			Common.gCode.return_to_origin();
			if (CVar.volatile.slow_motion_auto) {
				SVar.sim_rate = 5;
			} else {
				SVar.sim_rate = 40;
			}
			this.set_timer();
			SVar.sim_running = true;
		}
		if (CVar.track.force_zoom) {
			SVar.prev_zoom_ammount = Common.gTrack.scaleX;
			Common.gTrack.scaleX = Common.gTrack.scaleY = CVar.track.force_zoom_ammount;
		}
	}
	function update_sim(_ignorePause:Bool = false)
	{
		if (CVar.paused && !_ignorePause) return;
		if (CVar.volatile.hit_test_live) {
			trace("I need a new hit test deactivator!");
		}
		if (!this.rewind) {
			if (this.playback_rate > 40) {
				for (i in 0...Std.int(this.playback_rate / 40)) {
					Common.gRiderManager.advance_riders();
					++SVar.frames;
				}
			} else {
				Common.gRiderManager.advance_riders();
				++SVar.frames;
			}
		} else {
			if (this.playback_rate > 40) {
				for (i in 0...Std.int(this.playback_rate / 40)) {
					Common.gRiderManager.rewind_riders();
				}
			} else {
				Common.gRiderManager.rewind_riders();
			}
		}
		if (SVar.frames > SVar.max_frames) {
			SVar.max_frames = SVar.frames;
		}
		Common.gTimeline.update();
		this.update_camera();
		Common.gRiderManager.render();
	}
	function sup_frame_update() {
		Common.gRiderManager.sub_step_riders();
		Common.gRiderManager.render();
	}
	public function scrubberStepBack() {
		if (SVar.frames > 0) {
			Common.gRiderManager.rewind_riders();
		}
		if (SVar.frames == 0) {
			Common.gRiderManager.restore_start();
		}
		this.update_camera();
		Common.gRiderManager.render();
	}
	public function scrubberStepForward() {
		Common.gRiderManager.advance_riders();
		++SVar.frames;
		if (SVar.frames > SVar.max_frames) {
			SVar.max_frames = SVar.frames;
		}
		this.update_camera();
		Common.gRiderManager.render();
	}
	public function scrubberMassStepForward(_v:Int) {
		Common.gRiderManager.iterate_riders_loop(_v);
		SVar.frames += _v;
		if (SVar.frames > SVar.max_frames) {
			SVar.max_frames = SVar.frames;
		}
		this.update_camera();
		Common.gRiderManager.render();
	}
	public function end_sim()
	{
		if (SVar.sim_running || CVar.paused == true) {
			if (CVar.paused == true) CVar.paused = false;
			SVar.sim_running = false;
			SVar.frames_alt = SVar.frames;
			SVar.frames = 0;
			this.ff_loop = 1;
			this.iterator.stop();
			if (CVar.track.force_zoom) {
				Common.gTrack.scaleX = Common.gTrack.scaleY = SVar.prev_zoom_ammount;
			}
			if (CVar.volatile.flagged) {
				Common.gRiderManager.restore_flag();
			} else {
				Common.gRiderManager.restore_start();
			}
			Common.gRiderManager.render();
			Common.gRiderManager.set_rider_visual_stop();
		}
		Common.gRiderManager.render();
	}
	public function pause_sim()
	{
		SVar.frames_alt = SVar.frames;
		CVar.paused = true;
		SVar.pause_frame = SVar.frames;
	}
	public function resume_sim() {
		CVar.paused = false;
		SVar.pause_frame = null;
	}
	public function set_rider_start(_x:Float, _y:Float)
	{
		Common.gRiderManager.set_start(_x, _y);
		Common.gRiderManager.render();
	}
	function update_camera() 
	{
		if (SVar.sim_running) {
			this.camera.pan(CVar.track.rider_focus);
		}
	}
	public function reset() {
		Common.gRiderManager.destroy_flag();
		Common.gRiderManager.restore_start();
		Common.gRiderManager.render();
		CVar.volatile.flagged = false;
		this.flag_av = false;
	}
	public function decrease_playback_rate() {
		if (SVar.sim_rate == 1.25) return;
		this.iterator.stop();
		if (SVar.sim_rate <= 40 && this.ff_loop == 1) {
			SVar.sim_rate /= 2;
		} else {
			this.ff_loop /= 2;
		}
		this.set_timer();
	}
	public function increase_playback_rate() {
		if (this.ff_loop == 16) return;
		this.iterator.stop();
		if (SVar.sim_rate < 40) {
			SVar.sim_rate *= 2;
		} else {
			this.ff_loop *= 2;
		}
		this.set_timer();
	}
	public function set_timer() {
		this.iterator = new Timer(Std.int(1000 * ( 1 / SVar.sim_rate)));
		this.iterator.run = function():Void {
			for (a in 0...Std.int(this.ff_loop)) {
				this.update_sim();
				Common.gTextInfo.update_sim();
			}
		}
	}
	public function rewind_toggle() {
		if (this.rewind == false) {
			this.rewind = true;
		} else if (this.rewind != false){
			this.rewind = false;
		}
	}
	public function pause_play_toggle() {
		if (!SVar.sim_running && !CVar.paused) {
			Common.globalPlay();
		} else if (SVar.sim_running && !CVar.paused) {
			this.pause_sim();
		} else if (SVar.sim_running && CVar.paused) {
			this.resume_sim();
		}
	}
	public function step_forward() {
		if (!this.rewind) {
			this.update_sim(true);
		} else {
			this.rewind = false;
			this.update_sim(true);
			this.rewind = true;
		}
		this.update_camera();
		Common.gRiderManager.render();
	}
	public function step_backward() {
		if (this.rewind) {
			this.update_sim(true);
		} else {
			this.rewind = true;
			this.update_sim(true);
			this.rewind = false;
		}
		this.update_camera();
		Common.gRiderManager.render();
	}
	public function sub_step_forward() {
		
	}
	public function sub_step_backward() {
		
	}
	public function injectRiderPosition(_frame:Int) {
		Common.gRiderManager.inject_frame(_frame);
		this.update_camera();
		Common.gRiderManager.render();
	}
}