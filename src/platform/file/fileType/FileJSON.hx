package platform.file.fileType;

import haxe.Json;
import openfl.Lib;
import openfl.geom.Point;
import openfl.utils.Function;
import openfl.utils.Object;
import openfl.events.Event;

import global.Common;
import global.CVar;
import global.SVar;

import lr.lines.LineBase;

/**
 * ...
 * @author Kaelan Evans
 */
class FileJSON extends FileBase
{
	@:extern @:native("__hxcpp_set_float_format") //Credit to !Billy for finding this obscure solution to a problem that plagued me since April 2017
	static function changeFloat(format:String) {} //This has officially been fixed March 2018
	
	var chunk_lines:Array<Object>;
	var chunk_lines_array:Array<Array<Float>>;
	var step:Int = 0;
	var total:Int = 0;
	var chunk_size:Int = 500;
	public function new() {
		super();
	}
	override public function encode(?_name:String, ?_author:String, ?_description:String) {
		if (_name != null) this.name = _name;
		if (_author != null) this.author = _author;
		if (_description != null) this.description = _description;
		changeFloat("%.16f"); //set exact precision to 16 digits past decimal
		this.exportString = Json.stringify(this.parse_json(), null, "\t");
		changeFloat("%.15g"); //set back to default behavior
	}
	public function parse_json():Object //top object. Gets name, author, etc.
	{
		var _locArray = this.json_line_aray_parse();
		var _locRiderInfo = this.get_rider_info();
		var json_object:Object = {
			"label": this.name,
			"creator": this.author,
			"description": this.description,
			"version": "6.2",
			"startPosition": {
				"x": SVar.track_start_x,
				"y": SVar.track_start_y
			},
			"duration": SVar.max_frames,
			"lines": _locArray,
			"extra": { 
				"settings" : CVar.track,
				"rider_info" : _locRiderInfo,
			}
		}
		return(json_object);
	}
	function get_rider_info():Array<Object>
	{
		var _locArray:Array<Object> = new Array();
		var _indexCount:Int = 0;
		for (a in Common.gRiderManager.riderArray) {
			_locArray[_indexCount] = new Object();
			_locArray[_indexCount].pos_x = a.rider_pos_x;
			_locArray[_indexCount].pos_y = a.rider_pos_y;
			_locArray[_indexCount].color_a = a.color_a;
			_locArray[_indexCount].color_b = a.color_b;
			_locArray[_indexCount].name = a.rider_name;
			_locArray[_indexCount].velx = a.rider_x_velocity;
			_locArray[_indexCount].vely = a.rider_y_velocity;
			_locArray[_indexCount].angle = a.rider_angle;
			_locArray[_indexCount].scale = a.rider_scale;
			_locArray[_indexCount].spawn = a.spawn;
			_locArray[_indexCount].despawn = a.despawn;
			++_indexCount;
		}
		return (_locArray);
	}
	private function json_line_aray_parse():Array<Object> //parses line array and organizes data
	{
		var lines = Common.gGrid.lines;
		var a:Array<Object> = new Array();
		var line_Place_Override:Int = 0;
		for (i in lines) {
			if (i == null) {
				continue;
			}
			a[line_Place_Override] = new Object();
			a[line_Place_Override] = {
				"id": i.ID,
				"type": i.type,
				"x1": i.a.x,
				"y1": i.a.y,
				"x2": i.b.x,
				"y2": i.b.y,
				"flipped": i.inv,
				"leftExtended":  i.lExt,
				"rightExtended":  i.rExt
			};
			++line_Place_Override;
		}
		return(a);
	}
	override public function json_decode(_trackData:Object) {
		CVar.paused = true;
		SVar.track_start_x = _trackData.startPosition.x;
		SVar.track_start_y = _trackData.startPosition.y;
		Common.gRiderManager.set_start(SVar.track_start_x, SVar.track_start_y, 0);
		Common.gCode.return_to_origin(SVar.track_start_x, SVar.track_start_y);
		if (_trackData.label != null) {
			CVar.track.name = _trackData.label;
		}
		if (_trackData.lines != null) {
			this.cache_lines(_trackData);
		} else if (_trackData.linesArray != null) {
			this.cache_lines_array(_trackData);
		} else if (_trackData.linesArrayCompressed != null) {
			var _locDecompressed:String = this.decompress(_trackData.linesArrayCompressed);
		}
		if (_trackData.extra != null) {
			this.load_extra(_trackData.extra);
		}
	}
	function load_extra(_info:Object) 
	{
		CVar.track = _info.settings;
		if (_info.rider_info != null) {
			for (a in 0..._info.rider_info.length) {
				if (_info.rider_info[a] == null) continue;
				var color_a:Int = Common.randomRange(0, 0xFFFFFF);
				var color_b:Int = Common.randomRange(0, 0xFFFFFF);
				if (a != 0) {
					Common.gRiderManager.add_rider(2, 0, 0);
				}
				if (_info.rider_info[a].pos_x != null && _info.rider_info[a].pos_y != null) Common.gRiderManager.set_start(_info.rider_info[a].pos_x, _info.rider_info[a].pos_y, a);
				if (_info.rider_info[a].spawn != null) Common.gRiderManager.set_rider_spawn(a, _info.rider_info[a].spawn);
				if (_info.rider_info[a].despawn != null) Common.gRiderManager.set_rider_despawn(a, _info.rider_info[a].despawn);
				if (_info.rider_info[a].color_a != null) color_a = _info.rider_info[a].color_a;
				if (_info.rider_info[a].color_b != null) color_b = _info.rider_info[a].color_b;
				Common.gRiderManager.set_rider_colors(a, color_a, color_b);
				if (_info.rider_info[a].name != null) Common.gRiderManager.set_rider_name(a, _info.rider_info[a].name);
			}
		}
	}
	function cache_lines_array(_trackData:Object) {
		this.step = _trackData.linesArray.length;
		this.total = _trackData.linesArray.length;
		this.chunk_lines_array = _trackData.linesArray;
		this.chunk_lines_array.reverse();
		Lib.current.stage.addEventListener(Event.ENTER_FRAME, chunk_load_array);
	}
	function cache_lines(_trackData:Object) {
		this.step = _trackData.lines.length;
		this.total = _trackData.lines.length;
		this.chunk_lines = _trackData.lines;
		this.chunk_lines.reverse();
		Lib.current.stage.addEventListener(Event.ENTER_FRAME, chunk_load);
	}
	function chunk_load(e:Event):Void {
		Common.gTextInfo.textInfo_E.text = "" + Math.round(100 - (100 * (step / total))) + "%";
		Common.gTextInfo.render();
		for (i in 0...chunk_size) {
			var _loc1:LineBase;
			if (chunk_lines[step] == null) {
				--step;
				continue;
			}
			_loc1 = new LineBase(chunk_lines[step].type, new Point(chunk_lines[step].x1, chunk_lines[step].y1), new Point(chunk_lines[step].x2, chunk_lines[step].y2), chunk_lines[step].flipped);
			_loc1.ID = chunk_lines[step].id;
			_loc1.set_lim(this.get_lim_bool(chunk_lines[step].leftExtended, chunk_lines[step].rightExtended));
			Common.gGrid.cacheLine(_loc1);
			--step;
			if (step < 0) {
				Lib.current.stage.removeEventListener(Event.ENTER_FRAME, chunk_load);
				Common.gCode.set_load(false);
				CVar.paused = false;
				Common.gTextInfo.textInfo_E.text = "";
				break;
			}
		}
	}
	function chunk_load_array(e:Event):Void {
		for (i in 0...chunk_size) {
			var _loc1:LineBase;
			if (chunk_lines_array[step] == null) {
				--step;
				continue;
			}
			_loc1 = new LineBase(Std.int(this.chunk_lines_array[step][0]), new Point(this.chunk_lines_array[step][2], this.chunk_lines_array[step][3]), new Point(this.chunk_lines_array[step][4], this.chunk_lines_array[step][5]), this.get_bool_int(Std.int(this.chunk_lines_array[step][6])));
			_loc1.ID = Std.int(this.chunk_lines_array[step][1]);
			_loc1.set_lim(this.get_lim_int(Std.int(this.chunk_lines_array[step][7]), Std.int(this.chunk_lines_array[step][8])));
			Common.gGrid.cacheLine(_loc1);
			--step;
			if (step < 0) {
				Lib.current.stage.removeEventListener(Event.ENTER_FRAME, chunk_load_array);
				Common.gCode.set_load(false);
				CVar.paused = false;
				break;
			}
		}
	}
	//LZ-String decompression
	var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var f = String.fromCharCode;
	function decompress(compressed:String):String {
		if (compressed == null) return "";
		if (compressed == "") return null;
		return _decompress(compressed.length, 32768, function(index) { return compressed.charAt(index); });
	}
	function _decompress(length:Int, resetValue:Int, getNextValue:Function):String {
		
		var dictionary:Array<String> = [];
		var next;
		var enlargeIn:Int = 4;
		var dictSize:Int = 4;
		var numBits:Int = 3;
		var entry:String = "";
		var result = [];
		var i;
		var w;
		var bits, resb, maxpower, power;
		var c:String = "";
		var data:Object = {"val" : getNextValue(0), "position" : resetValue, "index" : 1};
		
		for (i in 0...3) {
			dictionary[i] = "" + i;
		}
		
		bits = 0;
		maxpower = Math.pow(2, 2);
		power = 1;
		while (power != maxpower) {
			resb = data.val & data.position;
			data.position >>= 1;
			if (data.position == 0) {
				data.position = resetValue;
				data.val = getNextValue(data.index++);
			}
			bits |= (resb > 0 ? 1 : 0) * power;
			power <<= 1;
		}
		switch (next = bits) {
			case 0:
				bits = 0;
				maxpower = Math.pow(2, 8);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}
				c = f(bits);
			case 1:
				bits = 0;
				maxpower = Math.pow(2,16);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}
				c = f(bits);
			case 2:
				return "";
		}
		
		dictionary[3] = c;
		w = c;
		result.push(c);
		
		while (true) {
			if (data.index > length) return "";
			
		}
		return("blep");
	}
}