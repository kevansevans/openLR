package platform.file.fileType;

import haxe.io.Bytes;
import openfl.utils.ByteArray;
import openfl.utils.Object;

/**
 * ...
 * @author Kaelan Evans
 */
class FileBase 
{
	public var data:Object;
	var name:String;
	var author:String;
	var description:String;
	var fileName:String;
	public var exportString:String = "";
	public var exportBytes:ByteArray;
	public function new() 
	{
		
	}
	public function encode(_name:String = "", _author:String = "", _description:String = "") {
		
	}
	public function json_decode(_obj:Object) {
		
	}
	public function lrpk_decode(_data:Bytes) {
		
	}
	public function get_lim_bool(l:Bool, r:Bool):Int {
		if (!l && !r) {
			return(0);
		} else if (l && !r) {
			return(1);
		} else if (!l && r) {
			return(2);
		} else if (l && r) {
			return(3);
		} else {
			return(0);
		}
	}
	public function get_lim_int(_a:Null<Int>, _b:Null<Int>):Int {
		var a = _a == null ? 0 : _a;
		var b = _b == null ? 0 : _b;
		if (a == 0 && b == 0) {
			return(0);
		} else if (a == 1 && b == 0) {
			return(1);
		} else if (a == 0 && b == 1) {
			return(2);
		} else if (a == 1 && b == 1) {
			return(3);
		} else {
			return (0);
		}	
	}
	public function get_bool_int(_v:Null<Int>):Bool {
		if (_v == null || _v == 0) {
			return false;
		} else {
			return true;
		}
	}
	public function stringFloat(_v:Float):String 
	{
		return "" + _v;
	}
}
