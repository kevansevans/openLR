package platform.file.importing;

import flash.utils.ByteArray;
import haxe.Json;
import openfl.utils.Object;
import platform.file.ImportBase;

import flash.filesystem.File;
import flash.events.Event;

import global.Common;
import platform.file.fileType.FileJSON;


/**
 * ...
 * @author kevansevans
 */
class ImportAir extends ImportBase
{
	var file:File;
	public function new() 
	{
		super();
	}
	public function importDocumentFile(_path:String) {
		file = File.documentsDirectory.resolvePath("openLR/saves/" + _path);
		file.addEventListener(Event.COMPLETE, load_air);
		file.load();
	}
	function load_air(e:Event) {
		var bytes:ByteArray = file.data;
		var decoded:Object = Json.parse(bytes.readUTFBytes(bytes.length));
		var json = new FileJSON();
		json.json_decode(decoded);
	}
}