package;

//Primary
import openfl.Assets;
#if sys
	import sys.FileSystem;
	import lime.system.System;
#end
import openfl.events.Event;
import openfl.display.FPS;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Point;
import openfl.Lib;

//third party

//OLR
import components.CheckBox;
import components.WindowBox;
import global.Common;
import global.CVar;
import global.engine.RiderManager;
import global.SVar;
import lr.scene.TextInfo;
import lr.scene.Track;
import lr.tool.Toolbar;
import lr.tool.ToolBase;
import lr.scene.timeline.TimelineControl;
import platform.ControlBase;
import platform.control.KeyControl;
import platform.control.MouseControl;

/**
 * ...
 * @author Kaelan Evans
 * 
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * //OpenLR Project Release Alpha 0.0.6
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * 
 * This program was built using HaxeDevelop IDE, with haxe and openFL. Other libraries if used can be found in project.xml
 * 
 */

class Main extends Sprite
{
	private var welcome:WindowBox;
	private var controlScheme:ControlBase; //Touch/Mouse detection
	private var KeyboardControl:KeyControl; //Global keyboard control
	private var track:Track;
	private var riders:RiderManager;
	private var toolBar:Toolbar;
	private var textInfo:TextInfo;
	private var timeline:TimelineControl;
	private var fps:FPS;
	public var loadingIcon:Sprite; //need a better place for this
	
	public function new() 
	{
		super();
		//Main is not a button
		mouseEnabled = false;
		
		Assets.loadLibrary("assets").onComplete(function(library) {
			this.launch();
		});
		
		#if (js || flash)
			Lib.current.stage.showDefaultContextMenu = false;
		#end
	}
	function launch():Void {
		
		Common.gCode = this; //This class, allows for easy access across rest of program. Common.gCode.doTheThingYouNeed();
		
		this.fps = new FPS(5, 5);
		this.addChild(this.fps);
		
		CVar.loadLocal();
		
		this.start();
	}
	public function start() {
		
		this.KeyboardControl = new KeyControl();
		this.controlScheme = new MouseControl();
		
		this.init_env(); //Establishes first needed events
		this.init_track(); //Establishes what the player will be interacting with
		
		Lib.current.stage.application.onExit.add (function (exitCode) {
			//Autosave code here
		});
		
		if (CVar.local.welcome #if (debug)|| true #end) {
			this.welcome = new WindowBox("Welcome!", WindowMode.MENU, 300);
			this.welcome.set_message_string("Welcome to Open Line Rider!\nVersion 0.0.6\n\nTo get started, left click and drag to draw a track for Bosh to ride. Press ► at the bottom to watch it in action! You can then press ■ to resume normal editing. You can switch to other tools on the top for easier track making.\n\nHappy riding!\n\nOpenLR is built with the Haxe toolkit, utilizing the OpenFL framework. Contributions to the project are welcome at: https://github.com/kevansevans/openLR", true);
			this.addChild(this.welcome);
			
			var never_again:CheckBox;
			never_again = new CheckBox("Show this box on startup", true);
			never_again.onChange = function() {
				CVar.local.welcome = never_again.value;
			}
			this.welcome.add_item(never_again);
			
			this.welcome.x = (Lib.current.stage.stageWidth / 4) - (this.welcome.width / 2);
			this.welcome.y = (Lib.current.stage.stageHeight / 2) - (this.welcome.height / 2);
			
			this.welcome.negative.func_up = function(e:MouseEvent) {
				CVar.flushLocal(); 
				this.removeChild(this.welcome); 
				this.welcome = null; //this never shows again, so let garbage collector handle it
			}
			this.welcome.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent) {
				Common.gToolBase.set_tool("None");
			});
			this.welcome.addEventListener(MouseEvent.MOUSE_OUT, function(e:MouseEvent) {
				Common.gToolBase.set_tool(ToolBase.lastTool);
			});
			
			this.welcome.displayItems();
		}
	}
	public function init_env() //Initialize enviornment
	{
		#if cpp
			this.init_paths(); //Make paths in case they're not present
		#end
		Lib.current.stage.addEventListener(Event.RESIZE, this.align);
	}
	public function init_track() //display minimum items
	{
		this.track = new Track();
		this.addChild(this.track);
		this.track.x = Lib.current.stage.stageWidth * 0.5;
		this.track.y = Lib.current.stage.stageHeight * 0.5;
		this.track.scaleX = this.track.scaleY = 2;
		
		this.riders = new RiderManager();
		
		this.riders.add_rider(2, 0, 0); //Duplicate this line for more riders. (//type, //x_pos, //y_pos)
		
		this.toolBar = new Toolbar(); //Tools to use
		this.addChild(this.toolBar);
		
		this.textInfo = new TextInfo(); //Displays track info
		this.addChild(this.textInfo);
		
		this.timeline = new TimelineControl(); //AKA Scrubber
		Lib.current.stage.addChild(this.timeline);
		this.timeline.update();
		
		this.align(); //Ensure everything comes in clean
	}
	public function reset_timeline() { //Probably can be relocated
		SVar.frames = 0;
		SVar.max_frames = 0;
		SVar.pause_frame = null;
		SVar.slow_motion = false;
		CVar.volatile.slow_motion_rate = 5;
		Lib.current.stage.removeChild(this.timeline);
		this.timeline = new TimelineControl();
		Lib.current.stage.addChild(this.timeline);
		this.timeline.update();
		this.timeline.x = (Lib.current.stage.stageWidth * 0.5) - (640);
		this.timeline.y = Lib.current.stage.stageHeight - 40;
	}
	public function set_load(_v:Bool) { //Shows loading animation
		this.toolBar.visible = !_v;
		this.timeline.visible = !_v;
		if (_v) {
			Toolbar.tool.set_tool("None");
			this.loadingIcon = Assets.getMovieClip("assets:Loader");
			this.addChild(this.loadingIcon);
			this.loadingIcon.x = 15;
			this.loadingIcon.y = 25;
		} else {
			Toolbar.tool.set_tool("Pencil");
			this.removeChild(this.loadingIcon);
			this.return_to_origin(SVar.track_start_x, SVar.track_start_y);
		}
	}
	public function align(e:Event = null) { //Adjusts position of elements
		this.toolBar.x = (Lib.current.stage.stageWidth / 2);
		this.toolBar.y = 2;
		
		this.textInfo.render();
		
		if (Common.gCamera != null) {
			Common.gCamera.update_pan_bounds();
		}
		
		this.timeline.x = (Lib.current.stage.stageWidth * 0.5) - (640);
		this.timeline.y = Lib.current.stage.stageHeight - 40;
		
		Common.stage_tl = new Point(0, 0);
		Common.stage_br = new Point(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
	}
	public function return_to_origin(_x:Float = 0, _y:Float = 0) { //Return to start. Currently can not handle moving to a position other than (0, 0)
		this.track.x = -(_x * SVar.track_scale);
		this.track.y = -(_y * SVar.track_scale);
		//it's close but no cigar.
	}
	public function jump_to_position(_x:Float, _y:Float, ?_scale:Float) { //not working at the moment
		this.track.x = _x;
		this.track.y = _y;
		
		if (_scale != null) this.track.scaleX = this.track.scaleY = _scale;
	}
	public function take_screencap() { //Needs to be reimplemented

	}
	public function end_screencap() {
		
	}
	#if cpp
		function init_paths() 
		{
			if (!FileSystem.isDirectory(System.documentsDirectory + "/openLR/")) FileSystem.createDirectory(System.documentsDirectory + "/openLR/");
			if (!FileSystem.isDirectory(System.documentsDirectory + "/openLR/saves")) FileSystem.createDirectory(System.documentsDirectory + "/openLR/saves");
		}
	#end
}