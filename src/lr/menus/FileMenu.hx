package lr.menus;

import haxe.io.Bytes;
import haxe.io.BytesData;
import openfl.Lib;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.Event;
import lime.system.System;

import components.CheckBox;
import components.Label;
import components.WindowBox;
import components.WindowBoxTabs;
import components.FileList;
import global.Common;
import lr.tool.ToolBase;
import components.IconButton;
import global.CVar;
import global.SVar;
import platform.file.fileType.FileJSON;
#if (cpp)
	import sys.io.FileOutput;
	import sys.io.File;
	import sys.FileSystem;
#end
#if sys
	import platform.file.importing.ImportSys;
#elseif air
	import platform.file.importing.ImportAir;
#end

/**
 * ...
 * @author Kaelan Evans
 */
enum abstract Method(Int) from Int {
	public var local_write:Int;
	public var local_load:Int;
	public var cloud_download:Int;
	public var cloud_upload:Int;
	public var import_nonjson:Int;
}
class FileMenu extends Sprite
{
	public static var current:FileInfoIcon;
	
	public var window:WindowBoxTabs;
	
	public var confirm_save_safety:WindowBox;
	public var successful_save:WindowBox;
	
	public var local_save:IconButton;
	public var track_name:Label;
	public var track_author:Label;
	public var track_description:Label;
	public var confirm_save:IconButton;
	public var cloud_save:CheckBox;
	
	public var local_load:IconButton;
	public var confirm:IconButton;
	public var delete:IconButton;
	public var list:FileList;
	
	public function new() 
	{
		super();
		
		Common.gLoadMenu = this;
		
		this.addEventListener(MouseEvent.MOUSE_OVER, this.temToolDis);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.temToolDisDis);
		
		this.window = new WindowBoxTabs("Save track", WindowMode.MENU, 335, 500);
		this.window.drag = true;
		this.addChild(this.window);
		
		//local save
		
		this.local_save = new IconButton("save");
		this.window.add_tool_icon(this.local_save);
		
		this.track_name = new Label(LabelType.INPUT, CVar.track.name, 310);
		this.window.add_item_to_group(track_name, Method.local_write, false, false, 0, 10);
		
		this.track_author = new Label(LabelType.INPUT, CVar.track.author, 310);
		this.window.add_item_to_group(track_author, Method.local_write, true);
		
		this.track_description = new Label(LabelType.INPUT_BOX, CVar.track.description, 310, 150);
		this.window.add_item_to_group(track_description, Method.local_write, true);
		
		this.confirm_save = new IconButton(Icon.yes);
		this.window.add_item_to_group(confirm_save, Method.local_write, true);
		
		this.cloud_save = new CheckBox("Upload to linerider.com", false);
		//this.window.add_item_to_group(cloud_save, Method.local_write);
		
		//Local load
		
		this.local_load = new IconButton("load");
		this.window.add_tool_icon(this.local_load);
		
		#if (flash && !air)
			this.list = new FileList("savedLinesOLR");
		#elseif (air)
			FileMenu.current = new FileInfoIcon("No save selected");
			FileMenu.current.bg.enabled = false;
			this.list = new FileList("openLR/saves");
			if (this.list.iconList.length > 5) {
				this.addEventListener(MouseEvent.MOUSE_WHEEL, this.list.scrolllist);
			}
		#elseif js
			this.list = new FileList("savedLinesOLR");
		#elseif sys
			FileMenu.current = new FileInfoIcon("No File Selected");
			FileMenu.current.bg.enabled = false;
			this.list = new FileList(System.documentsDirectory + "/openLR/saves");
			if (this.list.iconList.length > 5) {
				this.addEventListener(MouseEvent.MOUSE_WHEEL, this.list.scrolllist);
			}
		#end
		
		this.window.add_item_to_group(FileMenu.current, Method.local_load, false, false, 0, 5);
		
		this.confirm = new IconButton(Icon.yes);
		this.window.add_item_to_group(this.confirm, Method.local_load, false, false, 0, -25);
		
		this.delete = new IconButton(Icon.trash);
		this.window.add_item_to_group(this.delete, Method.local_load);
		
		this.window.add_item_to_group(this.list, Method.local_load, true);
		
		this.window.update_group(0);
		
		this.apply_listeners();
	}
	public function refresh():Void 
	{
		this.track_name.value = CVar.track.name;
		this.track_author.value = CVar.track.author;
		this.track_description.value = CVar.track.description;
		
		if (SVar.lineCount == 0) {
			this.window.update_group(Method.local_load);
			this.window.title = "Load locally saved track";
		}
		apply_listeners();
	}
	function temToolDisDis(e:MouseEvent):Void 
	{
		Common.gToolBase.set_tool(ToolBase.lastTool);
	}
	function temToolDis(e:MouseEvent):Void 
	{
		Common.gToolBase.set_tool("None");
	}
	public function update_item(_file:FileInfoIcon) {
		FileMenu.current.update(_file);
	}
	function apply_listeners() {
		this.local_save.func_up = function(e:MouseEvent) {
			this.window.update_group(Method.local_write);
			this.window.title = "Save track";
			apply_listeners();
		}
		this.local_load.func_up = function (e:MouseEvent) {
			this.window.update_group(Method.local_load);
			this.window.title = "Load locally saved track";
			apply_listeners();
		}
		this.confirm_save.func_up = function(e:MouseEvent) {
			#if (sys && !hl)
			this.export_track_pre();
			#end
		}
		this.confirm.func_up = function(e:MouseEvent):Void {
			if (FileMenu.current.type == SaveType.none) return;
			Common.gTrack.clear_stage();
			Common.gRiderManager.rider_wipe();
			#if (sys)
				this.load_sys(System.documentsDirectory + "/openLR/saves/" + FileMenu.current.info);
			#elseif (air)
				this.load_air_documents(FileMenu.current.info);
			#end
			this.window.negative.func_up(e);
		}
	}
	#if sys
	public function load_sys(_path:String) {
		var importer = new ImportSys();
		importer.load(_path);
	}
	#elseif (air)
	public function load_air_documents(_path:String) {
		var importer = new ImportAir();
		importer.importDocumentFile(_path);
	}
	public function load_air_localhost(_iitem_index:Int) {
		
	}
	#end
	#if (sys && !hl)
	function export_track_pre() {
		if (CVar.track.last_load != this.track_name.value && FileSystem.isDirectory(System.documentsDirectory + "/openLR/saves/" + this.track_name.value)) {
			this.window.mouseChildren = false;
			this.confirm_save_safety = new WindowBox("", WindowMode.CONFIRM);
			this.confirm_save_safety.set_message_string('This track doesn\'t appear to to be the same as \"' + this.track_name.value + '\", are you sure you\'d like to save in this location? This may cause future issues loading in the correct track, and will need to be manually fixed if problems do occur. If not, please choose a name that does not exist.');
			Lib.current.stage.addChild(this.confirm_save_safety);
			this.confirm_save_safety.x = (Lib.current.stage.stageWidth / 2) - (this.confirm_save_safety.width / 2);
			this.confirm_save_safety.y = (Lib.current.stage.stageHeight / 2) - (this.confirm_save_safety.height / 2);
			this.confirm_save_safety.negative.func_up = function(e:MouseEvent) {
				return_to_save_menu();
			}
			this.confirm_save_safety.positive.func_up = function(e:MouseEvent) {
				export_track_post();
				return_to_save_menu();
			}
		} else {
			export_track_post();
		}
	}
	function export_track_post() {
		var item_count:Int = 0;
		var path:String = "";
		if (FileSystem.isDirectory(System.documentsDirectory + "/openLR/saves/" + this.track_name.value)) {
			item_count = FileSystem.readDirectory(System.documentsDirectory + "/openLR/saves/" + this.track_name.value).length;
			path = System.documentsDirectory + "/openLR/saves/" + this.track_name.value + "/" + this.track_name.value + " " + item_count + ".track.json";
			while (FileSystem.exists(path)) {
				++item_count;
				path = System.documentsDirectory + "/openLR/saves/" + this.track_name.value + "/" + this.track_name.value + " " + item_count + ".track.json";
			}
		} else {
			FileSystem.createDirectory(System.documentsDirectory + "/openLR/saves/" + this.track_name.value);
			path = System.documentsDirectory + "/openLR/saves/" + this.track_name.value + "/" + this.track_name.value + " " + item_count + ".track.json";
		}
		
		CVar.track.author = this.track_author.value;
		CVar.track.name = this.track_name.value;
		CVar.track.last_load = this.track_name.value;
		CVar.track.description = this.track_description.value;
		
		this.window.mouseChildren = true;
		var loc_json = new FileJSON();
		loc_json.encode(this.track_name.value, this.track_author.value, this.track_description.value);
		var file = File.write(path);
		file.writeString(loc_json.exportString);
		file.close();
		
		this.window.negative.func_up(null);
	}
	#end
	function return_to_save_menu() {
		Lib.current.stage.removeChild(this.confirm_save_safety);
		this.window.mouseChildren = true;
	}
}