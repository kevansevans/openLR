package lr.menus;

import openfl.events.Event;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.FocusEvent;

import global.Common;
import global.SVar;
import global.CVar;
import lr.tool.ToolBase;
import components.IconButton;
import components.WindowBox;
import components.HSlider;
import components.CheckBox;
import components.Label;

/**
 * ...
 * @author Kaelan Evans
 */
class StartpointMenu extends Sprite
{
	public static var cameras:Array<CheckBox> = new Array();
	
	var window:WindowBox;
	var window_remove:WindowBox;
	
	var remove_rider:IconButton;
	
	var index:Int = -1;
	
	var exit_button:IconButton;
	
	var input_name:Label;
	
	var swatch_a:Swatch;
	var swatch_hex_a:Label;
	
	var slider_ra:HSlider;
	var slider_ga:HSlider;
	var slider_ba:HSlider;
	
	var slider_rb:HSlider;
	var slider_gb:HSlider;
	var slider_bb:HSlider;
	
	var swatch_b:Swatch;
	var swatch_hex_b:Label;
	
	var color_a:Int;
	var color_b:Int;
	
	var color_ra:Int;
	var color_ga:Int;
	var color_ba:Int;
	
	var color_rb:Int;
	var color_gb:Int;
	var color_bb:Int;
	
	var slider_rider_angle:HSlider;
	var skeleton:CheckBox;
	var velocity:CheckBox;
	var scarf:CheckBox;
	var alpha_slider:HSlider;
	var focus:CheckBox;
	var set_spawn:IconButton;
	var spawn_label:Label;
	var set_despawn:IconButton;
	var despawn_label:Label;
	
	public function new(_index:Int, _name:String, _exit:Dynamic, _colors:Array<Int>) 
	{
		super();
		
		this.mouseEnabled = false;
		
		this.window = new WindowBox("Rider properties #" + (_index + 1), WindowMode.MENU, 300);
		this.window.negative.addEventListener(MouseEvent.CLICK, _exit);
		this.window.drag = true;
		this.addChild(this.window);
		
		this.index = _index;
		
		this.addEventListener(MouseEvent.MOUSE_OVER, this.temToolDis);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.temToolDisDis);
		
		this.color_a = _colors[0];
		this.color_b = _colors[1];
		
		this.swatch_a = new Swatch();
		this.swatch_b = new Swatch();
		
		this.color_ra = (this.color_a >> 16) & 0xff;
		this.color_ga = (this.color_a >> 8) & 0xff;
		this.color_ba = this.color_a & 0xff;
		
		this.color_rb = (this.color_b >> 16) & 0xff;
		this.color_gb = (this.color_b >> 8) & 0xff;
		this.color_bb = this.color_b & 0xff;
		
		//this.mouseChildren = false;
		
		this.input_name = new Label(LabelType.INPUT, _name, 200);
		this.input_name.onChange = function():Void {
			this.set_rider_name(this.input_name.value);
		}
		this.input_name.onFocusShift = function(e:FocusEvent) {
			switch (e.type) {
				case (FocusEvent.FOCUS_IN) :
					SVar.label_focus = true;
				case (FocusEvent.FOCUS_OUT) :
					SVar.label_focus = false;
			}
		}
		this.window.add_item(this.input_name, false, false);
		
		this.remove_rider = new IconButton(Icon.trash);
		this.window.add_item(this.remove_rider);
		this.remove_rider.func_up = function (e:MouseEvent){
			this.confirm_deletion();
		}
		
		this.window.add_item(this.swatch_a, true, false);
		this.swatch_a.update(this.color_a);
		
		this.swatch_hex_a = new Label(LabelType.INPUT, "D51515", 100);
		this.window.add_item(this.swatch_hex_a, false, true);
		this.swatch_hex_a.onChange = function():Void {
			this.update_hex_a();
		}
		this.swatch_hex_a.onFocusShift = function(e:FocusEvent) {
			switch (e.type) {
				case (FocusEvent.FOCUS_IN) :
					SVar.label_focus = true;
				case (FocusEvent.FOCUS_OUT) :
					SVar.label_focus = false;
			}
		}
		
		this.slider_ra = new HSlider(0, 255, this.color_ra);
		this.slider_ra.setColors(0xFF0000, 0xFFFFFF, 0);
		this.window.add_item(this.slider_ra);
		this.slider_ra.onChange = function() {
			this.set_color_ra(Std.int(this.slider_ra.value));
		}
		
		this.slider_ga = new HSlider(0, 255, this.color_ga);
		this.slider_ga.setColors(0x00FF00, 0xFFFFFF, 0);
		this.window.add_item(this.slider_ga);
		this.slider_ga.onChange = function():Void {
			this.set_color_ga(Std.int(this.slider_ga.value));
		}
		
		this.slider_ba = new HSlider(0, 255, this.color_ba);
		this.slider_ba.setColors(0x0000FF, 0xFFFFFF, 0);
		this.window.add_item(this.slider_ba, false, true);
		this.slider_ba.onChange = function():Void {
			this.set_color_ba(Std.int(this.slider_ba.value));
		}
		
		this.window.add_item(this.swatch_b);
		this.swatch_b.update(this.color_b);
		
		this.swatch_hex_b = new Label(LabelType.INPUT, "FFFFFF", 100);
		this.window.add_item(this.swatch_hex_b, false, true);
		this.swatch_hex_b.onChange = function():Void {
			this.update_hex_b();
		}
		this.swatch_hex_b.onFocusShift = function(e:FocusEvent) {
			switch (e.type) {
				case (FocusEvent.FOCUS_IN) :
					SVar.label_focus = true;
				case (FocusEvent.FOCUS_OUT) :
					SVar.label_focus = false;
			}
		}
		
		this.slider_rb = new HSlider(0, 255, this.color_rb);
		this.slider_rb.setColors(0xFF0000, 0xFFFFFF, 0);
		this.window.add_item(this.slider_rb);
		this.slider_rb.onChange = function():Void {
			this.set_color_rb(Std.int(this.slider_rb.value));
		}
		
		this.slider_gb = new HSlider(0, 255, this.color_gb);
		this.slider_gb.setColors(0x00FF00, 0xFFFFFF, 0);
		this.window.add_item(this.slider_gb);
		this.slider_gb.onChange = function():Void {
			this.set_color_gb(Std.int(this.slider_gb.value));
		}
		
		this.slider_bb = new HSlider(0, 255, this.color_bb);
		this.slider_bb.setColors(0x0000FF, 0xFFFFFF, 0);
		this.window.add_item(this.slider_bb, false, true);
		this.slider_bb.onChange = function():Void {
			this.set_color_bb(Std.int(this.slider_bb.value));
		}
		
		this.scarf = new CheckBox("Render Scarf", true);
		this.window.add_item(this.scarf);
		this.scarf.onChange = function():Void {
			Common.gRiderManager.set_rider_scarf(this.index, this.scarf.value);
		}
		
		this.skeleton = new CheckBox("Render Skeleton", false);
		this.window.add_item(this.skeleton);
		this.skeleton.onChange = function():Void {
			Common.gRiderManager.set_rider_skeleton(this.index, this.skeleton.value);
		}
		
		this.velocity = new CheckBox("Render Velocity", false);
		//this.window.add_item(this.velocity);
		this.velocity.onChange = function():Void {
			Common.gRiderManager.set_rider_velocity(this.index, this.velocity.value);
		}
		
		this.alpha_slider = new HSlider(0, 1, 1, RoundMode.NONE);
		this.alpha_slider.setColors();
		this.window.add_item(this.alpha_slider);
		this.alpha_slider.onChange = function():Void {
			Common.gRiderManager.set_rider_alpha(this.index, this.alpha_slider.value);
		}
		
		this.slider_rider_angle = new HSlider(0, 360, 0, RoundMode.FLOOR);
		this.slider_rider_angle.setColors();
		//this.window.add_item(this.slider_rider_angle, true, true);
		this.slider_rider_angle.onChange = function():Void {
			Common.gRiderManager.set_rider_angle(this.index, (this.slider_rider_angle.value));
			trace(this.slider_rider_angle.value);
		}
		
		this.focus = new CheckBox("Camera focus", CVar.track.rider_focus == this.index ? true : false);
		this.window.add_item(this.focus, true, true);
		StartpointMenu.cameras.push(this.focus);
		this.focus.onChange = function():Void {
			this.update_camera_focus();
		}
		
		this.set_spawn = new IconButton(Icon.add);
		this.set_spawn.func_up = function(e:MouseEvent) {
			this.set_rider_spawn();
		}
		this.window.add_item(this.set_spawn);
		
		this.spawn_label = new Label();
		this.spawn_label.value = Common.gRiderManager.riderArray[this.index].spawn == null ? "No spawn frame" : Common.timeStamp(Common.gRiderManager.riderArray[this.index].spawn);
		this.window.add_item(this.spawn_label, false, true);
		this.spawn_label.onFocusShift = function(e:FocusEvent) {
			switch (e.type) {
				case (FocusEvent.FOCUS_IN) :
					SVar.label_focus = true;
				case (FocusEvent.FOCUS_OUT) :
					SVar.label_focus = false;
			}
		}
		
		this.set_despawn = new IconButton(Icon.remove);
		this.set_despawn.func_up = function(e:MouseEvent) {
			this.set_rider_despawn();
		}
		this.window.add_item(this.set_despawn);
		
		this.despawn_label = new Label();
		this.despawn_label.value = Common.gRiderManager.riderArray[this.index].despawn == null ? "No despawn frame" : Common.timeStamp(Common.gRiderManager.riderArray[this.index].despawn);
		this.window.add_item(this.despawn_label);
		this.despawn_label.onFocusShift = function(e:FocusEvent) {
			switch (e.type) {
				case (FocusEvent.FOCUS_IN) :
					SVar.label_focus = true;
				case (FocusEvent.FOCUS_OUT) :
					SVar.label_focus = false;
			}
		}
		
		this.window.displayItems();
	}
	
	function something(e:Event):Void 
	{
		trace(e);
	}
	function set_rider_spawn() 
	{
		if (SVar.frames == 0) return;
		Common.gRiderManager.set_rider_spawn(this.index, Common.gRiderManager.riderArray[this.index].spawn == null ? SVar.frames : null);
		this.spawn_label.value = Common.gRiderManager.riderArray[this.index].spawn == null ? "No spawn frame" : Common.timeStamp(Common.gRiderManager.riderArray[this.index].spawn);
	}
	function set_rider_despawn() 
	{
		if (SVar.frames == 0 && Common.gRiderManager.riderArray[this.index].despawn != null) return;
		Common.gRiderManager.set_rider_despawn(this.index, Common.gRiderManager.riderArray[this.index].despawn == null ? SVar.frames : null);
		this.despawn_label.value = Common.gRiderManager.riderArray[this.index].despawn == null ? "No despawn frame" : Common.timeStamp(Common.gRiderManager.riderArray[this.index].despawn);
	}
	
	function update_camera_focus() 
	{
		for (a in StartpointMenu.cameras) {
			a.set(false);
		}
		this.focus.set(true);
		CVar.track.rider_focus = this.index;
	}
	function confirm_deletion() 
	{
		this.window.visible = false;
		this.window_remove = new WindowBox("Delete rider?", WindowMode.CONFIRM, 255, 150);
		this.addChild(this.window_remove);
		this.window_remove.set_message_string('Are you sure you want to remove the rider ${this.input_name.value}? This action can not be undone.');
		this.window_remove.positive.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
			this.positive_delete();
		});
		this.window_remove.negative.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
			this.negative_delete();
		});
	}
	
	function negative_delete()
	{
		this.removeChild(this.window_remove);
		this.window.visible = true;
	}
	function positive_delete()
	{
		this.removeChild(this.window_remove);
		Common.gRiderManager.remove_rider(Common.gRiderManager.riderArray[this.index]);
	}
	function update_hex_a() 
	{
		var _locHexA:Int = Std.parseInt("0x" + this.swatch_hex_a.value);
		this.set_color_ra((_locHexA >> 16) & 0xFF, false);
		this.set_color_ga((_locHexA >> 8) & 0xFF, false);
		this.set_color_ba((_locHexA) & 0xFF, false);
		
		this.slider_ra.set(this.color_ra);
		this.slider_ba.set(this.color_ba);
		this.slider_ga.set(this.color_ga);
	}
	function update_hex_b() 
	{
		var _locHexA:Int = Std.parseInt("0x" + this.swatch_hex_b.value);
		this.set_color_rb((_locHexA >> 16) & 0xFF, false);
		this.set_color_gb((_locHexA >> 8) & 0xFF, false);
		this.set_color_bb((_locHexA) & 0xFF, false);
		
		this.slider_rb.set(this.color_rb);
		this.slider_bb.set(this.color_bb);
		this.slider_gb.set(this.color_gb);
	}
	function set_rider_name(_name:String) {
		Common.gRiderManager.set_rider_name(this.index, _name);
	}
	function temToolDisDis(e:Event):Void 
	{
		Common.gToolBase.set_tool(ToolBase.lastTool);
	}
	function temToolDis(e:Event):Void 
	{
		Common.gToolBase.set_tool("None");
	}
	function set_color_ra(_v:Int, _textUpdate:Bool = true) {
		this.update_color_a(_v, this.color_ga, this.color_ba, _textUpdate);
		this.color_ra = _v;
	}
	function set_color_ga(_v:Int, _textUpdate:Bool = true) {
		this.update_color_a(this.color_ra, _v, this.color_ba, _textUpdate);
		color_ga = _v;
	}
	function set_color_ba(_v:Int, _textUpdate:Bool = true) {
		this.update_color_a(this.color_ra, this.color_ga, _v, _textUpdate);
		color_ba = _v;
	}
	function update_color_a(_r:Int, _g:Int, _b:Int, _textUpdate:Bool = true) {
		color_a = Common.rgb_to_hex(_r, _g, _b);
		this.swatch_a.update(this.color_a);
		Common.gRiderManager.set_rider_colors(this.index, this.color_a, this.color_b);
		if (_textUpdate) {
			this.swatch_hex_a.set(StringTools.hex(this.color_a, 6));
		}
	}
	function set_color_rb(_v:Int, _textUpdate:Bool = true) {
		this.update_color_b(_v, this.color_gb, this.color_bb, _textUpdate);
		this.color_rb = _v;
	}
	function set_color_gb(_v:Int, _textUpdate:Bool = true) {
		this.update_color_b(this.color_rb, _v, this.color_bb, _textUpdate);
		color_gb = _v;
	}
	function set_color_bb(_v:Int, _textUpdate:Bool = true) {
		this.update_color_b(this.color_rb, this.color_gb, _v, _textUpdate);
		color_bb = _v;
	}
	function update_color_b(_r:Int, _g:Int, _b:Int, _textUpdate:Bool = true) {
		color_b = Common.rgb_to_hex(_r, _g, _b);
		this.swatch_b.update(this.color_b);
		Common.gRiderManager.set_rider_colors(this.index, this.color_a, this.color_b);
		if (_textUpdate) {
			this.swatch_hex_b.set(StringTools.hex(this.color_b, 6));
		}
	}
}
class Swatch extends Sprite {
	public function new(_fill:Int = 0) {
		super();
		this.update(_fill);
	}
	public function update(_fill:Int) {
		this.graphics.clear();
		this.graphics.lineStyle(1, 0);
		this.graphics.beginFill(_fill, 1);
		this.graphics.moveTo(0, 0);
		this.graphics.lineTo(25, 0);
		this.graphics.lineTo(25, 25);
		this.graphics.lineTo(0, 25);
		this.graphics.lineTo(0, 0);
	}
}