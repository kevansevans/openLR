package lr.scene;

import openfl.Assets;
import openfl.Lib;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

import global.Common;
import global.CVar;
import global.SVar;

/**
 * ...
 * @author Kaelan Evans
 * 
 * This class controls the text seen on the top right. When sim mode is added, it'll display time. Might switch it to use the color indicators instead of "Floor, Accel, Scene"
 * 
 */
class TextInfo extends Sprite
{
	var font:TextFormat = new TextFormat(Assets.getFont("fonts/Verdana.ttf").fontName, 12, 0, null, null, null, null, null, TextFormatAlign.RIGHT);
	public var textInfo_A:TextField;
	public var textInfo_B:TextField;
	public var textInfo_C:TextField;
	public var textInfo_D:TextField;
	public var textInfo_E:TextField;
	public var textInfo_F:TextField;
	
	private var textInfo_List:Array<TextField>;
	
	public function new() 
	{
		super();
		Common.gTextInfo = this;
		
		this.mouseEnabled = false;
		this.mouseChildren = false;
		
		this.textInfo_List = new Array();
		
		textInfo_A = new TextField();
		textInfo_B = new TextField();
		textInfo_C = new TextField();
		textInfo_D = new TextField();
		textInfo_E = new TextField();
		textInfo_F = new TextField();
		
		this.textInfo_List.push(this.textInfo_A);
		this.textInfo_List.push(this.textInfo_B);
		this.textInfo_List.push(this.textInfo_C);
		this.textInfo_List.push(this.textInfo_D);
		this.textInfo_List.push(this.textInfo_E);
		this.textInfo_List.push(this.textInfo_F);
		
		for (a in textInfo_List) {
			a.height = 18;
			a.selectable = false;
			a.defaultTextFormat = this.font;
			this.addChild(a);
		}
		
		this.textInfo_B.y = 15;
		this.textInfo_C.y = 30;
		this.textInfo_D.y = 45;
		this.textInfo_E.y = 60;
		this.textInfo_F.y = 75;
		
		this.update();
	}
	public function update() {
		textInfo_A.text = SVar.lineCount + " Lines";
		textInfo_B.text = SVar.lineCount_blue + " Floor";
		textInfo_C.text = SVar.lineCount_red + " Accel";
		textInfo_D.text = SVar.lineCount_green + " Scene";
		
		this.render();
	}
	public function update_sim() {
		var _locTime:String = Common.timeStamp(SVar.frames);
		
		textInfo_A.text = CVar.track.name;
		textInfo_B.text = _locTime + SVar.playbackModifierString;
		textInfo_C.text = SVar.rider_speed + " P/F";
		textInfo_D.text = "";
		
		this.render();
	}
	public function render() 
	{
		var _width:Float = 0;
		var _heightMod = 0;
		for (a in textInfo_List) {
			a.width = a.textWidth + 2;
			if (a.width > _width) {
				_width = a.width;
			}
			if (a.text == "") {
				_heightMod += 15;
			}
		}
		for (b in textInfo_List) {
			b.x = _width - b.width;
		}
		this.graphics.clear();
		this.graphics.beginFill(0xCCCCCC, 0.75);
		this.graphics.moveTo(-5, -2);
		this.graphics.lineTo(_width + 5, -2);
		this.graphics.lineTo(_width + 5, (this.textInfo_List.length * 15) - _heightMod + 5);
		this.graphics.lineTo(-5, (this.textInfo_List.length * 15) - _heightMod + 5);
		this.graphics.lineTo(-5, -2);
		
		this.x = Lib.current.stage.stageWidth - this.width - 2;
		this.y = 5;
	}
}