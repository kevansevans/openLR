package lr.scene.timeline;

import haxe.ds.Vector;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.text.TextField;

import global.Common;
import global.SVar;

/**
 * ...
 * @author Kaelan Evans
 * 
 * permits frame by frame scrubbing on the fly
 */
@:enum abstract Color(Int) from Int to Int {
	public var black:Int = 0;
	public var red:Int = 0xFF0000;
	public var pause:Int = 0x00FF00;
	public var current:Int = 0x0066FF;
	public var flag:Int = 0xCC0000;
}
class Ticker extends Sprite
{
	public var tickArray:Vector<Tick>;
	var timeStamp:TextField;
	public function new() 
	{
		super();
		
		this.tickArray = new Vector(320);
		for (i in 0...320) {
			this.tickArray[i] = new Tick(i);
			this.addChild(this.tickArray[i]);
			this.tickArray[i].x = i * 4;
			this.tickArray[i].mouseEnabled = false;
		}

		this.addEventListener(MouseEvent.MOUSE_OVER, this.enable_ticks);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.disable_ticks);
	}
	
	function disable_ticks(e:MouseEvent = null):Void 
	{
		for (a in this.tickArray) {
			a.mouseEnabled = false;
		}
	}
	function enable_ticks(e:MouseEvent):Void 
	{
		for (a in this.tickArray) {
			a.mouseEnabled = true;
		}
	}
	
	public function update() {
		for (i in 0...320) {
			this.tickArray[i].update(SVar.frames + i);
		}
	}
}
class Tick extends Sprite
{
	var frame:Int = 0;
	var root_id:Null<Int>;
	var timestamp:TextField;
	var show_time:Bool = false;
	public function new(_id:Int) 
	{
		super();
		
		this.frame = _id;
		this.root_id = _id;
		this.graphics.clear();
		this.graphics.lineStyle(4, 0, 1);
		this.graphics.moveTo(0, 0);
		this.graphics.lineTo(0, 25);
		
		this.timestamp = new TextField();
		this.timestamp.y = 25;
		this.timestamp.mouseEnabled = false;
		this.addChild(this.timestamp);
		
		if (this.root_id != 0 && this.root_id != 319) {
			this.addEventListener(MouseEvent.MOUSE_OVER, update_time);
			this.addEventListener(MouseEvent.MOUSE_OUT, hide_time);
			this.timestamp.text = "";
		}
	}
	public function update(_id) {
		this.show_time = false;
		this.graphics.clear();
		this.frame = _id - 160;
		this.graphics.moveTo(0, 0);
		if (this.frame < 0) {
			this.graphics.lineStyle(4, Color.red, 0.1);
		} else if (this.frame > SVar.max_frames) {
			this.graphics.lineStyle(4, Color.black, 0.5);
		} else if (this.frame == SVar.frames) {
			this.graphics.lineStyle(4, Color.current, 1);
		} else if (this.frame == SVar.pause_frame) {
			this.graphics.lineStyle(4, Color.pause, 1);
			this.show_time = true;
		} else if (this.frame == SVar.flagged_frame) {
			this.graphics.lineStyle(4, Color.flag, 1);
			this.show_time = true;
		}
		else {
			if (this.frame % 10 == 1) {
				this.graphics.lineStyle(4, Color.black, 0.8);
			} else { 
				this.graphics.lineStyle(4, Color.black, 0.4);
			}
		}
		if (this.frame == SVar.max_frames) {
			this.show_time = true;
		}
		if (!this.show_time) this.timestamp.text = "";
		this.graphics.lineTo(0, 25);
		this.timestamp.width = this.timestamp.textWidth;
		this.timestamp.x = -(this.timestamp.width / 2);
		this.update_time();
		if (this.root_id == 0) this.timestamp.x = 2;
		if (this.root_id == 319) this.timestamp.x = -(timestamp.width + 2);
	}
	function hide_time(e:MouseEvent):Void 
	{
		if (this.root_id != 0 && this.root_id != 319) {
			if (!this.show_time) this.timestamp.text = "";
		}
	}
	function update_time(e:MouseEvent = null):Void 
	{
		if (!this.show_time) this.timestamp.text = "";
		else this.timestamp.text = Common.timeStamp(this.frame);
	}
}