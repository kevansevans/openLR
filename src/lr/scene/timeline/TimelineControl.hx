package lr.scene.timeline;

import openfl.Lib;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Point;

import components.IconButton;
import components.IconBar;
import lr.scene.timeline.Ticker;
import lr.tool.Toolbar;
import lr.tool.ToolBase;
import global.Common;
import global.SVar;
import global.CVar;

/**
 * ...
 * @author Kaelan Evans
 */
class TimelineControl extends Sprite
{
	var ticker:Ticker;
	var ticker_pause:Bool = false;
	var isOver:Bool = false;
	
	var bar_playback:IconBar;
	
	var add:IconButton;
	//var remove:IconButton;
	var pause:IconButton;
	var playB:IconButton;
	var stop:IconButton;
	var flag:IconButton;
	
	public function new() 
	{
		super();
		Common.gTimeline = this;
		
		this.graphics.clear();
		this.graphics.beginFill(0xAAAAAA, 0.75);
		this.graphics.moveTo( 0, -3);
		this.graphics.lineTo(1280, -3);
		this.graphics.lineTo(1280, 27);
		this.graphics.lineTo( 0, 27);
		
		this.buttonMode = true;
		this.doubleClickEnabled = true;
		
		this.ticker = new Ticker();
		this.addChild(this.ticker);
		this.ticker.mouseEnabled = false;
		
		this.addEventListener(MouseEvent.MOUSE_OVER, this.hover);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.resume);
		
		this.bar_playback = new IconBar();
		
		this.pause = new IconButton(Icon.pause);
		this.pause.func_up = function(e:MouseEvent) {
			Common.gSimManager.pause_play_toggle();
			if (!CVar.paused) {
				this.pause.deselect();
				this.playB.select();
			} else if (SVar.sim_running && CVar.paused) {
				this.pause.select();
				this.playB.deselect();
			}
			this.stop.deselect();
		}
		this.bar_playback.add_icon(this.pause);
		
		this.playB = new IconButton(Icon.play);
		this.playB.func_up = function(e:MouseEvent) {
			Common.gSimManager.init_sim();
			this.pause.deselect();
			this.playB.select();
			this.stop.deselect();
		}
		this.bar_playback.add_icon(this.playB);
		
		this.stop = new IconButton(Icon.stop);
		this.stop.func_up = function(e:MouseEvent) {
			Common.gSimManager.close_sim();
			this.pause.deselect();
			this.playB.deselect();
			this.stop.select();
		}
		this.bar_playback.add_icon(this.stop);
		this.stop.select();
		
		this.flag = new IconButton(Icon.flag);
		this.flag.func_up = function(e:MouseEvent) {
			if (!CVar.mod_shift) Common.gRiderManager.set_flag();
			else Common.gRiderManager.set_flag(true);
		}
		this.bar_playback.add_icon(this.flag);
		
		this.addChild(this.bar_playback);
		this.bar_playback.update();
		this.bar_playback.x = (this.width / 2) - (this.bar_playback.width);
		this.bar_playback.y = -35;
	}
	function hover(e:MouseEvent):Void 
	{
		if (Common.gToolBase.currentTool.leftMouseIsDown) return;
		this.addEventListener(MouseEvent.MOUSE_DOWN, this.downAction);
		this.addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, this.downActionRight);
		this.disableTool();
	}
	function resume(e:MouseEvent):Void 
	{
		if (Common.gToolBase.currentTool.leftMouseIsDown) return;
		this.removeEventListener(MouseEvent.MOUSE_DOWN, this.downAction);
		this.removeEventListener(MouseEvent.RIGHT_MOUSE_DOWN, this.downActionRight);
		this.reenableTool();
	}
	function downAction(e:MouseEvent):Void 
	{
		this.isOver = true;
		this.removeEventListener(MouseEvent.MOUSE_OUT, this.resume);
		this.addEventListener(MouseEvent.MOUSE_OVER, this.enterDown);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.leaveDown);
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_UP, this.release);
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.scrub);
	}
	function downActionRight(e:MouseEvent):Void 
	{
		this.isOver = true;
		this.removeEventListener(MouseEvent.MOUSE_OUT, this.resume);
		this.addEventListener(MouseEvent.MOUSE_OVER, this.enterDown);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.leaveDown);
		Lib.current.stage.addEventListener(MouseEvent.RIGHT_MOUSE_UP, this.release);
		Lib.current.stage.addEventListener(MouseEvent.MOUSE_MOVE, this.scrubRight);
	}
	function leaveDown(e:MouseEvent):Void 
	{
		this.isOver = false;
	}
	function enterDown(e:MouseEvent):Void 
	{
		this.isOver = true;
	}
	function release(e:MouseEvent):Void 
	{
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.scrub);
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.scrubRight);
		this.removeEventListener(MouseEvent.MOUSE_OVER, this.enterDown);
		this.removeEventListener(MouseEvent.MOUSE_OUT, this.leaveDown);
		this.addEventListener(MouseEvent.MOUSE_OVER, this.hover);
		this.addEventListener(MouseEvent.MOUSE_OUT, this.resume);
		if (this.isOver) {
			this.addEventListener(MouseEvent.MOUSE_DOWN, this.downAction);
		} else {
			this.reenableTool();
		}
	}
	function reenableTool() {
		Toolbar.tool.set_tool(ToolBase.lastTool); 
	}
	function disableTool() {
		Toolbar.tool.set_tool("None"); 
	}
	private var prevX:Float = 0; 
	function scrub(e:MouseEvent):Void 
	{
		var curX:Float = this.mouseX; 
		if (curX - prevX < -4) { 
			Common.gSimManager.scrubberStepForward(); 
			this.prevX = this.mouseX; 
		} else if (curX - prevX > 4) { 
			Common.gSimManager.scrubberStepBack(); 
			this.prevX = this.mouseX; 
		} 
		this.update(); 
		Common.gTextInfo.update_sim(); 
	}
	function scrubRight(e:MouseEvent):Void 
	{
		var curX:Float = this.mouseX;
		var value:Int = Std.int(Math.min(Math.round(SVar.max_frames / 80), 200));
		if (value == 0) value = 1;
		if (curX - prevX < -4) {
			if (SVar.frames == SVar.max_frames) {
				return;
			} else if (SVar.frames + value >= SVar.max_frames) {
				Common.gSimManager.scrubberMassStepForward(SVar.max_frames - SVar.frames);
			} else {
				Common.gSimManager.scrubberMassStepForward(value);
			}
			this.prevX = this.mouseX; 
		} else if (curX - prevX > 4) { 
			if (SVar.frames - value <= 0) {
				Common.gSimManager.injectRiderPosition(0);
			} else {
				Common.gSimManager.injectRiderPosition(SVar.frames - value);
			}
			this.prevX = this.mouseX; 
		} 
		this.update(); 
		Common.gTextInfo.update_sim(); 
	}
	public function update() {
		this.ticker.update();
	}
	function add_new_rider() {
		var center:Point = new Point(Lib.current.stage.stageWidth / 2, Lib.current.stage.stageHeight / 2);
		var trackLocal = Common.gTrack.globalToLocal(center);
		Common.gRiderManager.add_rider(2, trackLocal.x, trackLocal.y);
	}
}