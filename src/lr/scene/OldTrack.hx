package lr.scene;

import openfl.Lib;
import openfl.display.Sprite;
import openfl.geom.Point;

import lr.nodes.Grid;
import lr.nodes.Panel;
import lr.lines.LineBase;
import lr.rider.RiderBase;
import lr.rider.objects.FlagMarker;
import lr.rider.objects.StartPointVis;
import global.engine.SimManager;
import global.Common;
import global.CVar;
import global.SVar;

/**
 * ...
 * @author Kaelan Evans
 * 
 * Track movie clip with track specific functions and variables
 * 
 */
class Track extends Sprite
{
	private var grid:Grid;
	private var simManager:SimManager;
	public var canvas:Sprite;
	public var rider:Sprite;
	
	private var local_tl:Point = new Point(0, 0);
	private var local_br:Point = new Point(0, 0);
	private var local_br_grid:Point = new Point(0, 0);
	
	public var renderList:Array<Panel>;
	public var hitTestList:Array<LineBase>;
	
	public function new() 
	{
		super();
		Common.gTrack = this;
		SVar.track_scale = 1;
		this.grid = new Grid();
		this.simManager = new SimManager();
		
		this.canvas = new Sprite();
		this.addChild(this.canvas);
		this.rider = new Sprite();
		this.addChild(this.rider);
		
		this.renderList = new Array();
		this.update_visibility();
		
		this.mouseEnabled = false;
		this.canvas.mouseEnabled = false;
		this.rider.mouseEnabled = false;
	}
	var lowX:Int;
	var highX:Int;
	var lowY:Int;
	var highY:Int;
	public function update_visibility(_ignorePositionCheck:Bool = false) 
	{
		this.local_br = new Point(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
		
		var _locTrackTL = this.globalToLocal(this.local_tl);
		var _locTrackBR = this.globalToLocal(this.local_br);
		var _gridPointA = Common.tilePos(_locTrackTL.x, _locTrackTL.y);
		var _gridPointB = Common.tilePos(_locTrackBR.x, _locTrackBR.y);
		var _locTrackTL_grid = new Point(_gridPointA.x, _gridPointA.y);
		var _locTrackBR_grid = new Point(_gridPointB.x, _gridPointB.y);
		
		if (_ignorePositionCheck == false) {
			if (this.local_br_grid.x == _locTrackTL_grid.x && this.local_br_grid.y == _locTrackTL_grid.y) {
				return;
			}
		}
		this.lowX = Std.int(_locTrackTL_grid.x - 2);
		this.highX = Std.int(_locTrackBR_grid.x + 2);
		this.lowY = Std.int(_locTrackTL_grid.y - 2);
		this.highY = Std.int(_locTrackBR_grid.y + 2);
		
		for (a in renderList) {
			if (a.panelPosX < lowX) {
				a.onStage = false;
				this.renderList.remove(a);
				continue;
			}
			if (a.panelPosX > highX) {
				a.onStage = false;
				this.renderList.remove(a);
				continue;
			}
			if (a.panelPosY < lowY) {
				a.onStage = false;
				this.renderList.remove(a);
				continue;
			}
			if (a.panelPosY > highY) {
				a.onStage = false;
				this.renderList.remove(a);
				continue;
			}
		}
		
		for (b in lowX...highX) {
			if (Grid.tile[b] == null) continue;
			for (c in lowY...highY) {
				if (Grid.tile[b][c] == null || Grid.tile[b][c].onStage) continue;
				Grid.tile[b][c].onStage = true;
				this.renderList.push(Grid.tile[b][c]);
			}
		}
		for (c in Common.gRiderManager.riderArray) {
			if (c == null) continue;
			this.update_rider(c, this.get_vis_bool(new Point(c.body.anchors[4].pos.x, c.body.anchors[4].pos.y)));
			this.update_start_point(c.start_point, this.get_vis_bool(new Point(c.start_point.x, c.start_point.y)));
			this.update_checkpoint(c.checkpoint, this.get_vis_bool(new Point(c.checkpoint.x, c.checkpoint.y)));
		}
	}
	
	function update_start_point(_start:StartPointVis, _v:Bool) 
	{
		if (SVar.sim_running) _v = false;
		if (_start.visible == _v) return;
		else _start.visible = _v;
	}
	function update_rider(_rider:RiderBase, _v:Bool) {
		if (_rider.clips.visible == _v) return;
		else _rider.clips.visible = _v;
	}
	function update_checkpoint(_flag:FlagMarker, _v:Bool) {
		if (!CVar.volatile.flagged) _v = false;
		if (_flag.visible == _v) return;
		else _flag.visible = _v;
	}
	public function get_vis_bool(_p:Point, _offset:Int = 0):Bool {
		var _locTilePos = Common.tilePos(_p.x, _p.y);
		if (_locTilePos.x >= lowX + _offset && _locTilePos.x <= highX - _offset && _locTilePos.y >= lowY + _offset && _locTilePos.y <= highY - _offset) {
			return true;
		}
		return false;
	}
	public function renderPreview(_line:LineBase)
	{
		this.graphics.clear();
		if (!CVar.volatile.preview_mode) {
			var _loc_3:Float = _line.n.x > 0 ? (Math.ceil(_line.n.x)) : (Math.floor(_line.n.x));
			var _loc_4:Float = _line.n.y > 0 ? (Math.ceil(_line.n.y)) : (Math.floor(_line.n.y));
			switch(_line.type) {
				case LineType.Floor: 
					this.graphics.lineStyle(2, 0x0066FF, 1, true, "normal", "round");
					this.graphics.moveTo(_line.a.x + _loc_3, _line.a.y + _loc_4);
					this.graphics.lineTo(_line.b.x + _loc_3, _line.b.y + _loc_4);
				case LineType.Accel: 
					this.graphics.lineStyle(2, 0xCC0000, 1, true, "normal", "round");
					this.graphics.beginFill(0xCC0000, 1);
					this.graphics.moveTo(_line.a.x + _loc_3, _line.a.y + _loc_4);
					this.graphics.lineTo(_line.b.x + _loc_3, _line.b.y + _loc_4);
					this.graphics.lineTo(_line.b.x + (_line.n.x * 5 - _line.d.x * _line.invDst * 5), _line.b.y + (_line.n.y * 5 - _line.d.y * _line.invDst * 5));
					this.graphics.lineTo(_line.b.x - _line.d.x * _line.invDst * 5, _line.b.y - _line.d.y * _line.invDst * 5);
					this.graphics.endFill();
				case LineType.Scene: 
					this.graphics.lineStyle(2, 0x00CC00, 1);
					this.graphics.moveTo(_line.a.x, _line.a.y);
					this.graphics.lineTo(_line.b.x, _line.b.y);
					return;
			}
		}
		this.graphics.lineStyle(2, 0x000000, 1);
		this.graphics.moveTo(_line.a.x, _line.a.y);
		this.graphics.lineTo(_line.b.x, _line.b.y);
	}
	public function clear_preview()
	{
		this.graphics.clear();
	}
	public function set_rendermode_play() {
		if (CVar.volatile.color_play) {
			SVar.lineColor = true;
		} else {
			SVar.lineColor = false;
		}
		this.updatePanels();
	}
	public function set_rendermode_edit() {
		if (CVar.volatile.preview_mode) {
			SVar.lineColor = false;
		} else {
			SVar.lineColor = true;
		}
		this.updatePanels();
	}
	public function updatePanels() {
		for (a in Grid.tile) {
			if (a == null) continue;
			for (b in a) {
				if (b == null) continue;
				
				b.update();
			}
		}
	}
	public function clear_stage()
	{
		for (a in Common.gGrid.lines) {
			if (a == null) {
				continue;
			}
			Common.gGrid.remove_line(a);
		}
		Common.gGrid.new_grid();
		Common.gSimManager.reset();
		Common.gRiderManager.rider_wipe();
		Common.gCode.reset_timeline();
		SVar.frames = 0;
		SVar.max_frames = 0;
		SVar.pause_frame = null;
		SVar.rider_speed_top = 0;
		CVar.volatile.slow_motion_rate = 5;
		SVar.slow_motion = false;
	}
	public function set_simmode_play(_fromStart:Bool = false) {
		this.simManager.start_sim(_fromStart);
		SVar.sim_running = true;
	}
	public function set_simmode_resume() {
		
	}
	public function set_simmode_pause() {
		this.simManager.pause_sim();
	}
	public function set_simmode_stop() {
		this.simManager.end_sim();
		SVar.sim_running = false;
	}
}