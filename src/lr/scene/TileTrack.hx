package lr.scene;

import haxe.ds.Vector;
import lr.lines.LineVis;
import openfl.geom.Rectangle;

import openfl.Assets;
import openfl.display.BitmapData;
import openfl.display.Tilemap;
import openfl.display.TileContainer;
import openfl.display.Tileset;

import global.Common;

/**
 * ...
 * @author ...
 */
class TileTrack extends Tilemap
{
	private var containers:Vector<TileContainer>;
	private var map:BitmapData;
	private var set:Tileset;
	public var scene_layer:TileContainer;
	public var color_layer:TileContainer;
	public var black_layer:TileContainer;
	public var object_layer:TileContainer;
	public var rider_layer:TileContainer;
	public var foreground_layer:TileContainer;
	
	public var pos_x(default, set):Float;
	public var pos_y(default, set):Float;
	public var drag(default, set):Bool = false;
	public var scale(default, set):Float = 1;
	
	public function new(_width:Int, _height:Int) 
	{
		this.map = Assets.getBitmapData("assets:TileBody");
		this.set = new Tileset(this.map);
		this.set.addRect(new Rectangle(0, 0, 20, 20)); //line
		
		super(_width, _height, this.set);
		
		Common.gTileTrack = this;
		
		this.containers = new Vector(6);
		
		this.scene_layer = new TileContainer();
		this.addTile(this.scene_layer);
		this.containers[0] = this.scene_layer;
		
		this.color_layer = new TileContainer();
		this.addTile(this.color_layer);
		this.containers[1] = this.color_layer;
		
		this.black_layer = new TileContainer();
		this.addTile(this.color_layer);
		this.containers[2] = this.black_layer;
		
		this.object_layer = new TileContainer();
		this.addTile(this.object_layer);
		this.containers[3] = this.object_layer;
		
		this.rider_layer = new TileContainer();
		this.addTile(this.rider_layer);
		this.containers[4] = this.rider_layer;
		
		this.foreground_layer = new TileContainer();
		this.addTile(this.foreground_layer);
		this.containers[5] = this.foreground_layer;
	}
	public function add_line(_line:LineVis) {
		trace("blep");
		//this.black_layer.addTile(_line);
	}
	function set_pos_x(_v:Float):Float {
		for (a in containers) {
			a.x = _v;
		}
		return pos_x = _v;
	}
	function set_pos_y(_v:Float):Float {
		for (a in containers) {
			a.y = _v;
		}
		return pos_y = _v;
	}
	function set_drag(_v:Bool):Bool {
		for (a in containers) {
			switch(_v) {
				case true :
					trace("start drag");
				case false :
					trace("stop drag");
			}
		}
		return _v;
	}
	function set_scale(_v:Float):Float {
		for (a in containers) {
			a.scaleX = a.scaleY = _v;
		}
		return scale = _v;
	}
}