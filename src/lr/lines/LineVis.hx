package lr.lines;

import openfl.Vector;
import openfl.display.BitmapData;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.geom.Point;

import global.Common;
import global.SVar;
import lr.lines.LineBase;

/**
 * ...
 * @author Kaelan Evans
 */
class LineVis extends Sprite
{
	static inline var res:Int = 32;
	static var fill_black:BitmapData = new BitmapData(res, res, false, 0);
	static var fill_blue:BitmapData = new BitmapData(res, res, true, 0xBD0066FF);
	static var fill_red:BitmapData = new BitmapData(res, res, true, 0xBDCC0000);
	static var fill_green:BitmapData = new BitmapData(res, res, true, 0xBD00CC00);
	
	static inline var THICKNESS:Float = 1; 
	
	var a:Point = new Point(0, 0);
	var b:Point = new Point(0, 0);
	var n:Point = new Point();
	var black:Shape;
	var color:Shape;
	var line:LineBase;
	var type:Null<Int>;
	public var panelList:Map<String, Bool>;
	public function new(_line:LineBase) 
	{
		super();
		
		this.panelList = new Map();
		
		this.line = _line;
		this.type = _line.type;
		
		this.b = _line.b.subtract(_line.a);
		
		this.black = new Shape();
		this.color = new Shape();
		
		this.alpha = 0;
		
		this.init_render();
		this.render();
		
		this.mouseEnabled = false;
	}
	public function update(_v:Bool, _loc:String) {
		switch (_v) {
			case true :
				for (a in panelList) {
					if (a == true) {
						this.panelList[_loc] = _v;
						return;
					}
				}
				this.panelList[_loc] = _v;
				//this.visible = true;
				this.alpha = 1;
				if (!Common.gTrack.canvas.contains(this)) {
					Common.gTrack.canvas.addChild(this);
				}
			case false :
				this.panelList[_loc] = _v;
				for (a in panelList) {
					if (a == true) {
						return;
					}
				}
				//this.visible = false;
				this.alpha = 0;
		}
	}
	function init_render() {
		this.n = new Point(this.line.n.x > 0 ? (Math.ceil(this.line.n.x)) : (Math.floor(this.line.n.x)), this.line.n.y > 0 ? (Math.ceil(this.line.n.y)) : (Math.floor(this.line.n.y)));
		
		this.black.graphics.beginBitmapFill(LineVis.fill_black);
		this.black.graphics.drawTriangles(line_tris());
		
		switch (this.type) {
			case (LineType.Floor) :
				this.color.graphics.beginBitmapFill(fill_blue);
			case (LineType.Accel) :
				this.color.graphics.beginBitmapFill(fill_red);
			case (LineType.Scene) :
				this.color.graphics.beginBitmapFill(fill_green);
		}
		this.color.graphics.drawTriangles(line_tris(true));

		this.addChild(this.color);
		this.addChild(this.black);
	}
	public function render() 
	{
		if (SVar.lineColor) {
			this.color.visible = true;
			if (this.type == 2) this.black.visible = false;
		} else {
			this.color.visible = false;
			if (this.type == 2) this.black.visible = true;
		}
	}
	function line_tris(_color:Bool = false):Vector<Float> 
	{
		var f_array:Array<Float> = new Array();
		//establish tris first
		//left/a cap lower tri
		f_array.push(0); 	//0
		f_array.push(0);	//1
		f_array.push(-THICKNESS);	//2
		f_array.push(0);	//3
		f_array.push(0);	//4
		f_array.push(THICKNESS);	//5
		//left/a cap upper tri
		f_array.push(0);	//6
		f_array.push(0);	//7
		f_array.push(-THICKNESS);	//8
		f_array.push(0);	//9
		f_array.push(0);	//10
		f_array.push(-THICKNESS);	//11
		//right/b cap lower tri
		f_array.push(0);	//12
		f_array.push(0);	//13
		f_array.push(THICKNESS);	//14
		f_array.push(0);	//15
		f_array.push(0);	//16
		f_array.push(THICKNESS);	//17
		//right/b cap upper tri
		f_array.push(0);	//18
		f_array.push(0);	//19
		f_array.push(THICKNESS);	//20
		f_array.push(0);	//21
		f_array.push(0);	//22
		f_array.push( -THICKNESS);	//23
		if (this.type == 1 && _color) {
			f_array.push(THICKNESS);
			f_array.push(0);
			f_array.push( -THICKNESS * 2);
			f_array.push( 0);
			f_array.push( -THICKNESS * 2);
			f_array.push( -THICKNESS * 2);
		}
		
		var r_array = rotate(f_array);
	
		for (a in 12...r_array.length) {
			if (a % 2 != 0) {
				r_array[a] += b.y;
			}
			else {
				r_array[a] += b.x;
			}
		}
		if (_color) {
			for (a in 0...r_array.length) {
				if (a % 2 != 0) {
					r_array[a] += n.y;
				}
				else {
					r_array[a] += n.x;
				}
			}
		}
		//fill
		r_array.push(r_array[4]);
		r_array.push(r_array[5]);
		r_array.push(r_array[10]);
		r_array.push(r_array[11]);
		r_array.push(r_array[16]);
		r_array.push(r_array[17]);
		r_array.push(r_array[10]);
		r_array.push(r_array[11]);
		r_array.push(r_array[16]);
		r_array.push(r_array[17]);
		r_array.push(r_array[22]);
		r_array.push(r_array[23]);
		
		var vector:Vector<Float>;
		
		return (vector = new Vector(r_array.length, true, r_array));
	}
	function rotate(_array:Array<Float>):Array<Float>
	{
		var angle = Common.get_angle_radians(this.a, this.b);
		var array:Array<Float> = new Array();
		for (a in 0..._array.length) {
			var b:Int;
			var f:Float;
			if (a % 2 != 0) {
				b = a - 1;
				f = (_array[a] * Math.cos(angle)) + (_array[b] * Math.sin(angle));
			}
			else {
				b = a + 1;
				f = (_array[a] * Math.cos(angle)) - (_array[b] * Math.sin(angle));
			}
			array.push(f);
		}
		return(array);
	}
}