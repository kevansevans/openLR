package lr.lines;

import lr.lines.collision.*;
import lr.rider.phys.anchors.CPoint;
import lr.nodes.Grid;
import global.Common;
import global.CVar;
import openfl.geom.Point;

/**
 * ...
 * @author Kaelan Evans
 * 
 * 
 * Base variables for all lines
 * 
 */
@:enum abstract LineType(Int) from Int to Int {
	public var None:Int = -1;
	public var Floor:Int = 0;
	public var Accel:Int = 1;
	public var Scene:Int = 2;
	public var Deccel:Int = 3;
}
@:enum abstract SwapType(Int) from Int to Int {
	public var CollisionCycle:Int = 0;
	public var SceneryToggle:Int = 1;
	public var InverseToggle:Int = 2;
	public var DirectionToggle:Int = 3;
}
class LineBase
{
	public static var zone = 10;
	public var mov:Int = 0;
	public var a:Point;
	public var b:Point;
	public var d:Point;
	public var n:Point;
	public var invSqrDis:Float;
	public var dst:Float;
	public var invDst:Float;
	public var LIM:Float;
	public var _lim:Float;
	public var _lim1:Float;
	public var _lim2:Float;
	public var inv:Bool = false;
	public var type:Int = -1;
	public var prevType:Int = -1;
	public var ID:Int = -1;
	public var gridList:Array<Array<Int>>;
	public var gridVisList:Array<Array<Int>>;
	public var prevLine:Int;
	public var nextLine:Int;
	public var lExt:Bool = false;
	public var rExt:Bool = false;
	public var lowest_collided_frame:Int = 0;
	public var acc:Point;
	public var accel_value:Float = 0.1;
	public var phys:Collision;
	public var hit:Bool = false;
	public var grind:Bool = false;
	public var vis:LineVis;
	
	public function new(_type:Int, _a:Point, _b:Point, _inv:Bool, _lim = -1)
	{
		this.gridList = new Array();
		this.gridVisList = new Array();
		this.type = _type;
		this.prevType = _type;
		this.a = _a;
		this.b = _b;
		this.inv = _inv;
		this.calculateConstants();
		this.set_lim(_lim == -1 ? (0) : (_lim));
		this.vis = new LineVis(this);
	}
	public function inject_grid_loc(a:Array<Int>)
	{
		this.gridList.push(a);
	}
	public function inject_grid_vis_loc(a:Array<Int>)
	{
		this.gridVisList.push(a);
	}
	function calculateConstants() 
	{
		d = b.subtract(a);
        var _loc2 = Math.pow(d.x, 2) + Math.pow(d.y, 2);
        invSqrDis = 1 / _loc2;
        dst = Math.sqrt(_loc2);
        invDst = 1 / dst;
		n = new Point(d.y * invDst * (inv ? (1) : ( -1)), d.x * invDst * (inv ? ( -1) : (1)));
        LIM = Math.min(0.25, LineBase.zone / dst);
		this.acc = new Point(n.y * this.accel_value * (this.inv ? (1) : (-1)), n.x * this.accel_value * (this.inv ? ( -1) : (1)));
		
		if (this.phys != null) {
			return;
		}
		switch(this.type) {
			case LineType.None :
				this.phys = new NoCollision();
			case LineType.Floor :
				this.phys = new Floor(this);
			case LineType.Accel :
				this.phys = new Acceleration(this);
			case LineType.Scene :
				return;
			default :
				this.phys = new NoCollision();
		}
	}
	public function set_lim(input:Float)
	{
		switch (input)
        {
            case 0:
            {
                _lim1 = 0;
                _lim2 = 1;
				lExt = false;
				rExt = false;
            } 
            case 1:
            {
                _lim1 = -LIM;
                _lim2 = 1;
				lExt = true;
				rExt = false;
            } 
            case 2:
            {
                _lim1 = 0;
                _lim2 = 1 + LIM;
				lExt = false;
				rExt = true;
            } 
            case 3:
            {
                _lim1 = -LIM;
                _lim2 = 1 + LIM;
				lExt = true;
				rExt = true;
            } 
        }
        _lim = input;
	}
	function addPrevLine(line, extend)
    {
        if (extend)
        {
            switch (this.get_lim())
            {
                case 0:
                {
                    this.set_lim(1);
                } 
                case 2:
                {
                    this.set_lim(3);
                } 
            }
        }
        prevLine = line.ID;
    }
    function addNextLine(line, extend)
    {
        if (extend)
        {
            switch (this.get_lim())
            {
                case 0:
                {
                    this.set_lim(2);
                } 
                case 1:
                {
                    this.set_lim(3);
                }
            }
        }
        nextLine = line.ID;
    }
	function get_lim()
    {
        return (_lim);
    } 
	public function collide(dot:CPoint) 
	{
		this.phys.collide(dot);
	}
	public function render_collide() 
	{
		if (!CVar.volatile.hit_test) return;
		trace("I need a new hit test!");
	}
	public function changeBehavior(_mode:Int) {
		switch (_mode) {
			case SwapType.CollisionCycle : //cycles through all line types
				switch (this.type) {
					case 0 :
						this.type = 1;
						this.prevType = 1;
						this.phys = new Acceleration(this);
					case 1 :
						this.type = 0;
						this.prevType = 0;
						this.phys = new Floor(this);
					case 2 :
						return;
					default :
						return;
				}
			case SwapType.DirectionToggle : //changes direction behavior
				switch (this.type) {
					case 1 :
						this.quickDirectionFlip();
						this.phys = new Acceleration(this);
					default :
						return;
				}
			case SwapType.InverseToggle : //flips collision side
				switch (this.type) {
					case 0 :
						this.inv = !this.inv;
					case 1 :
						this.inv = !this.inv;
					case 2 :
						return;
					default :
						return;
				}
			case SwapType.SceneryToggle :
				if (this.type != 2) {
					this.type = 2;
					this.phys = new NoCollision();
				} else {
					switch (this.prevType) {
					case -1 :
						this.type = 0;
						this.phys = new Floor(this);
					case 0 :
						this.type = 0;
						this.phys = new Floor(this);
					case 1 :
						this.type = 1;
						this.phys = new Acceleration(this);
					default :
						this.type = 0;
						this.phys = new Floor(this);
					}
				}
				Common.gGrid.updateRegistry(this);
		}
		this.calculateConstants();
	}
	function quickDirectionFlip() 
	{
		var tempX1 = this.a.x;
		var tempY1 = this.a.y;
		var tempX2 = this.b.x;
		var tempY2 = this.b.y;
			
		this.a.x = tempX2;
		this.a.y = tempX1;
		this.b.x = tempY2;
		this.b.y = tempY1;
		this.inv = !this.inv;
	}
}
class LinePreview extends LineBase
{

	public inline function new(_a:Point, _b:Point, _inv:Bool, _type:Int) 
	{
		super(_type, _a, _b, _inv);
	}
}