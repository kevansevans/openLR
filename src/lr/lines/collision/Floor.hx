package lr.lines.collision;

import lr.lines.LineBase;
import lr.lines.Collision;
import lr.rider.phys.anchors.CPoint;

/**
 * ...
 * @author Kaelan Evans
 */
class Floor extends Collision
{
	public function new(_line:LineBase) 
	{
		super();
		this.parent = _line;
	}
	override public function collide(dot:CPoint) {
		var _loc5:Float = dot.pos.x - parent.a.x;
        var _loc6:Float = dot.pos.y - parent.a.y;
        var _loc4:Float = parent.n.x * _loc5 + parent.n.y * _loc6;
        var _loc7:Float = (_loc5 * parent.d.x + _loc6 * parent.d.y) * parent.invSqrDis;
        if (dot.dir.x * parent.n.x + dot.dir.y * parent.n.y > 0)
        {
            if (_loc4 > 0 && _loc4 < LineBase.zone && _loc7 >= parent._lim1 && _loc7 <= parent._lim2)
            {
				parent.render_collide();
                dot.pos.x = dot.pos.x - _loc4 * parent.n.x;
                dot.pos.y = dot.pos.y - _loc4 * parent.n.y;
                dot.vel.x = dot.vel.x + parent.n.y * dot.fr * _loc4 * (dot.vel.x < dot.pos.x ? (1) : (-1));
                dot.vel.y = dot.vel.y - parent.n.x * dot.fr * _loc4 * (dot.vel.y < dot.pos.y ? ( -1) : (1));
                return;
            } // end if
        } // end if
	}
}