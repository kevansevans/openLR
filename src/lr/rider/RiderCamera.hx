package lr.rider;

import openfl.geom.Point;
import openfl.Lib;

import global.Common;
import global.SVar;
import global.engine.RiderManager;

/**
 * ...
 * @author ...
 */
class RiderCamera 
{
	private var left_bound:Float = Lib.current.stage.stageWidth * 0.38;
	private var right_bound:Float = Lib.current.stage.stageWidth * 0.61;
	private var top_bound:Float = Lib.current.stage.stageHeight * 0.38;
	private var bottom_bound:Float = Lib.current.stage.stageHeight * 0.61;
	
	public var rider_gx:Null<Int>;
	public var rider_gy:Null<Int>;
	
	public function new() 
	{
		Common.gCamera = this;
	}
	public function pan(_riderID:Int = 0) {
		if (Common.gRiderManager.riderArray[_riderID] == null) return;
		var _locPoint:Point = Common.gTrack.localToGlobal(new Point(Common.gRiderManager.riderArray[_riderID].body.anchors[4].pos.x, Common.gRiderManager.riderArray[_riderID].body.anchors[4].pos.y));
		var _locXPan:Float = 0;
		var _locYPan:Float = 0;
		
		if (_locPoint.x > this.right_bound) {
			_locXPan = this.right_bound - _locPoint.x;
		} else if (_locPoint.x < this.left_bound) {
			_locXPan = this.left_bound - _locPoint.x;
		}
		if (_locPoint.y > this.bottom_bound) {
			_locYPan = this.bottom_bound - _locPoint.y;
		} else if (_locPoint.y < this.top_bound) {
			_locYPan = this.top_bound - _locPoint.y;
		}
		var _locRiderPanel = Common.tilePos(Common.gRiderManager.riderArray[_riderID].body.anchors[4].pos.x, Common.gRiderManager.riderArray[_riderID].body.anchors[4].pos.y);
		if (_locRiderPanel.x != this.rider_gx || _locRiderPanel.y != this.rider_gy) {
			this.rider_gx = _locRiderPanel.x;
			this.rider_gy = _locRiderPanel.y;
		}
		Common.gTrack.x += _locXPan;
		Common.gTrack.y += _locYPan;
		SVar.rider_speed = RiderManager.speed[_riderID];
		if (SVar.rider_speed > SVar.rider_speed_top) SVar.rider_speed_top = SVar.rider_speed;
	}
	public function update_pan_bounds() {
		this.left_bound = Lib.current.stage.stageWidth * 0.38;
		this.right_bound =Lib.current.stage.stageWidth * 0.61;
		this.top_bound = Lib.current.stage.stageHeight * 0.38;
		this.bottom_bound = Lib.current.stage.stageHeight * 0.61;
	}
}