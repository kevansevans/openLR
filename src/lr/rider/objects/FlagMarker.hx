package lr.rider.objects;

import openfl.Assets;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.geom.ColorTransform;

import global.Common;

/**
 * ...
 * @author Kaelan Evans
 */
class FlagMarker extends Sprite
{
	private var time:TextField;
	private var index:Int;
	
	var flag_a:Sprite;
	var flag_b:Sprite;
	var font:TextFormat = new TextFormat(Assets.getFont("fonts/Verdana.ttf").fontName, 4, 0xFFFFFF, null, null, null, null, null, TextFormatAlign.RIGHT);
	public function new(_index:Int) 
	{
		super();
		
		this.index = _index;
		
		this.flag_a = new Sprite();
		this.flag_b = new Sprite();
		this.flag_a.addChild(Assets.getMovieClip("assets:start_a"));
		this.flag_b.addChild(Assets.getMovieClip("assets:start_b"));
		this.addChild(this.flag_a);
		this.addChild(this.flag_b);
		this.flag_b.y -= 11;
		this.scaleX = this.scaleY = 0.75;
		
		this.time = new TextField();
		this.addChild(this.time);
		this.time.x = -this.time.width - 1;
		this.time.y = -21;
		this.time.defaultTextFormat = font;
		this.time.selectable = false;
		
		this.mouseChildren = false;
		
		this.alpha = 0.75;
	}
	public function update(_frames:Int) {
		this.time.text = Common.timeStamp(_frames) + "\n" + Common.gRiderManager.riderArray[this.index].rider_name;
	}
	public function set_color(a:Int = 0xD51515, b = 0xFFFFFF) {
		this.flag_a.transform.colorTransform = new ColorTransform(((a >> 16) & 0xff) / 255, ((a >> 8) & 0xff) / 255, ((a & 0xff) / 255));
		this.flag_b.transform.colorTransform = new ColorTransform(((b >> 16) & 0xff) / 255, ((b >> 8) & 0xff) / 255, ((b & 0xff) / 255));
	}
}