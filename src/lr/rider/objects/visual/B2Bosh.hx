package lr.rider.objects.visual;

import openfl.Assets;
import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.display.LineScaleMode;
import openfl.geom.Point;
import openfl.geom.ColorTransform;

import global.Common;
import global.engine.RiderManager;
import lr.rider.RiderBase;
import lr.rider.phys.SkeletonBase;
import lr.rider.objects.VisBase;
import lr.rider.phys.frames.FrameBase;
import lr.rider.phys.scarf.ScarfBase;

/**
 * ...
 * @author Kaelan Evans
 */
class B2Bosh extends VisBase
{
	public var bosh:Sprite;
	
	private var color_a:Int = 0xD20202;
	private var color_b:Int = 0xFFFFFF;
	private var prevScale:Float;
	
	/*
	private var body_vis:Sprite;
	private var leftArm:Sprite;
	private var rightArm:Sprite;
	private var leftLeg:Sprite;
	private var rightLeg:Sprite;
	private var sled:Sprite;
	private var string:Shape;
	private var scarf_a:Sprite;
	private var scarf_b:Sprite;
	private var scarf_vis:Shape;
	private var skeleton_vis:Shape;*/
	
	private var body_vis:ShaderSprite;
	private var leftArm:ShaderSprite;
	private var rightArm:ShaderSprite;
	private var leftLeg:ShaderSprite;
	private var rightLeg:ShaderSprite;
	private var sled:ShaderSprite;
	private var string:Shape;
	private var scarf_a:ShaderSprite;
	private var scarf_b:ShaderSprite;
	private var scarf_vis:Shape;
	private var skeleton_vis:Shape;
	
	private var body:FrameBase;
	private var scarf:ScarfBase;
	private var skeleton:SkeletonBase;
	private var base:RiderBase;
	
	private var riderID:Int;
	
	private var clips:Array<Sprite>;
	private var vectors:Array<Shape>;
	
	public function new(_body:FrameBase, _scarf:ScarfBase, _skeleton:SkeletonBase, _base:RiderBase, _id:Int) 
	{
		super();
		
		this.riderID = _id;
		
		this.body = _body;
		this.scarf = _scarf;
		this.skeleton = _skeleton;
		
		this.getAssets();
	}
	private function getAssets() {
		
		this.clips = new Array();
		this.vectors = new Array();
		
		this.scarf_vis = new Shape();
		this.string = new Shape();
		this.skeleton_vis = new Shape();
		this.sled = new ShaderSprite("assets:olr_sled");
		this.body_vis = new ShaderSprite("assets:olr_body");
		this.scarf_a = new ShaderSprite("assets:scarf_a");
		this.scarf_b = new ShaderSprite("assets:scarf_b");
		this.leftLeg = new ShaderSprite("assets:olr_leg");
		this.rightLeg = new ShaderSprite("assets:olr_leg");
		this.leftArm = new ShaderSprite("assets:olr_arm");
		this.rightArm = new ShaderSprite("assets:olr_arm");
		
		this.addChild(this.scarf_vis);
		this.vectors.push(this.scarf_vis);
		this.addChild(this.leftArm);
		this.clips.push(this.leftArm);
		this.addChild(this.leftLeg);
		this.clips.push(this.leftLeg);
		this.addChild(this.sled);
		this.clips.push(this.sled);
		this.addChild(this.rightLeg);
		this.clips.push(this.rightLeg);
		this.addChild(this.body_vis);
		this.body_vis.addChild(this.scarf_b);
		this.scarf_b.x = 10.35;
		this.scarf_b.y = -5;
		this.body_vis.addChild(this.scarf_a);
		this.scarf_a.x = 10.35;
		this.scarf_a.y = -5;
		this.clips.push(this.body_vis);
		this.addChild(this.string);
		this.vectors.push(this.string);
		this.addChild(this.rightArm);
		this.clips.push(this.rightArm);
		this.addChild(this.skeleton_vis);
		this.vectors.push(this.skeleton_vis);
		
		for (a in clips) {
			a.scaleX = a.scaleY = 0.5;
			a.mouseEnabled = false;
		}
		
		this.mouseChildren = false;
		
		this.prevScale = Common.gTrack.scaleX;
	}
	var rider_alpha:Float = 1;
	override public function update_scale() 
	{
		
	}
	override public function render()
	{
		if (!Common.gRiderManager.riderArray[this.riderID].visible) return; 
		if (Common.gRiderManager.riderArray[this.riderID] != null) {
			this.rider_alpha = Common.gRiderManager.riderArray[this.riderID].rider_alpha;
		}
		this.body_vis.x = this.body.anchors[4].pos.x;
		this.body_vis.y = this.body.anchors[4].pos.y;
		this.body_vis.rotation = Common.get_angle_degrees(new Point(this.body.anchors[4].pos.x, this.body.anchors[4].pos.y), new Point(this.body.anchors[5].pos.x, this.body.anchors[5].pos.y));
		
		this.sled.x = this.body.anchors[0].pos.x;
		this.sled.y = this.body.anchors[0].pos.y;
		this.sled.rotation = Common.get_angle_degrees(new Point(this.body.anchors[0].pos.x, this.body.anchors[0].pos.y), new Point(this.body.anchors[3].pos.x, this.body.anchors[3].pos.y));
		
		this.leftArm.x = this.rightArm.x = this.body.anchors[5].pos.x;
		this.leftArm.y = this.rightArm.y = this.body.anchors[5].pos.y;
		this.leftArm.rotation = Common.get_angle_degrees(new Point(this.body.anchors[5].pos.x, this.body.anchors[5].pos.y), new Point(this.body.anchors[6].pos.x, this.body.anchors[6].pos.y));
		this.rightArm.rotation = Common.get_angle_degrees(new Point(this.body.anchors[5].pos.x, this.body.anchors[5].pos.y), new Point(this.body.anchors[7].pos.x, this.body.anchors[7].pos.y));
		
		this.leftLeg.x = this.rightLeg.x = this.body.anchors[4].pos.x;
		this.leftLeg.y = this.rightLeg.y = this.body.anchors[4].pos.y;
		this.leftLeg.rotation = Common.get_angle_degrees(new Point(this.body.anchors[4].pos.x, this.body.anchors[4].pos.y), new Point(this.body.anchors[8].pos.x, this.body.anchors[8].pos.y));
		this.rightLeg.rotation = Common.get_angle_degrees(new Point(this.body.anchors[4].pos.x, this.body.anchors[4].pos.y), new Point(this.body.anchors[9].pos.x, this.body.anchors[9].pos.y));
		
		//rider rendering
		for (a in clips) {
			a.alpha = this.rider_alpha;
		}
		this.string.graphics.clear();
		this.skeleton_vis.graphics.clear();
		this.scarf_vis.graphics.clear();
		if (!RiderManager.crash[this.riderID]) {
			//render_string();
		}
		if (Common.gRiderManager.riderArray[this.riderID] != null) {
			if (Common.gRiderManager.riderArray[this.riderID].bone_vis) {
				//this.render_bones();
			}
		}
		if (Common.gRiderManager.riderArray[this.riderID] != null) {
			if (Common.gRiderManager.riderArray[this.riderID].vel_vis) {
				//this.render_velocity();
			}
		}
		if (Common.gRiderManager.riderArray[this.riderID] != null) {
			if (Common.gRiderManager.riderArray[this.riderID].scarf_vis) {
				//this.render_scarf();
			}
		}
	}
	public function render_string() {
		#if (flash)
				this.string.graphics.lineStyle(1, 0, this.rider_alpha, false, LineScaleMode.NONE);
			#else
				this.string.graphics.lineStyle(0.5, 0, this.rider_alpha, false, LineScaleMode.NONE);
			#end
			this.string.graphics.moveTo(this.body.anchors[6].pos.x, this.body.anchors[6].pos.y);
			this.string.graphics.lineTo(this.body.anchors[3].pos.x, this.body.anchors[3].pos.y);
			this.string.graphics.lineTo(this.body.anchors[7].pos.x, this.body.anchors[7].pos.y);
	}
	public function render_scarf() 
	{
		#if (flash)
			this.scarf_vis.graphics.lineStyle(2, this.color_b, this.rider_alpha, false, LineScaleMode.NORMAL, "none");
		#else
			this.scarf_vis.graphics.lineStyle(2, this.color_b, this.rider_alpha, false, LineScaleMode.NONE, "none");
		#end
		this.scarf_vis.graphics.moveTo(this.scarf.edges[0].a.pos.x, this.scarf.edges[0].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[0].b.pos.x, this.scarf.edges[0].b.pos.y);
		this.scarf_vis.graphics.moveTo(this.scarf.edges[2].a.pos.x, this.scarf.edges[2].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[2].b.pos.x, this.scarf.edges[2].b.pos.y);
		this.scarf_vis.graphics.moveTo(this.scarf.edges[4].a.pos.x, this.scarf.edges[4].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[4].b.pos.x, this.scarf.edges[4].b.pos.y);
		#if (flash)
			this.scarf_vis.graphics.lineStyle(2, this.color_a, this.rider_alpha, false, LineScaleMode.NORMAL, "none");
		#else
			this.scarf_vis.graphics.lineStyle(2, this.color_a, this.rider_alpha, false, LineScaleMode.NONE, "none");
		#end
		this.scarf_vis.graphics.moveTo(this.scarf.edges[1].a.pos.x, this.scarf.edges[1].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[1].b.pos.x, this.scarf.edges[1].b.pos.y);
		this.scarf_vis.graphics.moveTo(this.scarf.edges[3].a.pos.x, this.scarf.edges[3].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[3].b.pos.x, this.scarf.edges[3].b.pos.y);
		this.scarf_vis.graphics.moveTo(this.scarf.edges[5].a.pos.x, this.scarf.edges[5].a.pos.y);
		this.scarf_vis.graphics.lineTo(this.scarf.edges[5].b.pos.x, this.scarf.edges[5].b.pos.y);
	}
	override public function set_scarf_color(a:Int, b:Int) {
		this.color_a = a;
		this.color_b = b;
		
		this.scarf_a.transform.colorTransform = new ColorTransform(((a >> 16) & 0xff) / 255, ((a >> 8) & 0xff) / 255, ((a & 0xff) / 255));
		this.scarf_b.transform.colorTransform = new ColorTransform(((b >> 16) & 0xff) / 255, ((b >> 8) & 0xff) / 255, ((b & 0xff) / 255));
	}
	public function render_bones() {
		#if (flash)
			this.skeleton_vis.graphics.lineStyle(1, 0xFF6600, 0.5, false, LineScaleMode.NORMAL);
		#else
			this.skeleton_vis.graphics.lineStyle(0.25, 0xFF6600, 1, false, LineScaleMode.NONE);
		#end
		for (i in 0...4) { //Minimal sled points
			this.skeleton_vis.graphics.moveTo(this.skeleton.edges[i].a.pos.x, this.skeleton.edges[i].a.pos.y);
			this.skeleton_vis.graphics.lineTo(this.skeleton.edges[i].b.pos.x, this.skeleton.edges[i].b.pos.y);
		}
		#if (flash)
			this.skeleton_vis.graphics.lineStyle(1, 0xCC0033, 0.5, false, LineScaleMode.NORMAL);
		#else
			this.skeleton_vis.graphics.lineStyle(0.25, 0xCC0033, 1, false, LineScaleMode.NONE);
		#end
		for (i in 9...14) { //Minimal body_vis points
			this.skeleton_vis.graphics.moveTo(this.skeleton.edges[i].a.pos.x, this.skeleton.edges[i].a.pos.y);
			this.skeleton_vis.graphics.lineTo(this.skeleton.edges[i].b.pos.x, this.skeleton.edges[i].b.pos.y);
		}
		#if (flash)
			this.skeleton_vis.graphics.lineStyle(0.25, 0x6600FF, 1, false, LineScaleMode.NORMAL);
		#else
			this.skeleton_vis.graphics.lineStyle(0.25, 0x6600FF, 0.1, false, LineScaleMode.NONE);
		#end
		for (i in 0...this.body.anchors.length) {
			this.skeleton_vis.graphics.beginFill(0x6600ff, 1);
			this.skeleton_vis.graphics.drawCircle(this.body.anchors[i].pos.x, this.body.anchors[i].pos.y, 0.5);
			this.skeleton_vis.graphics.endFill();
		}
	}
	public function render_velocity() {
		#if (flash)
			this.skeleton_vis.graphics.lineStyle(1, 0x0000FF, 0.5, false, LineScaleMode.NORMAL);
		#else
			this.skeleton_vis.graphics.lineStyle(0.25, 0x0000FF, 1, false, LineScaleMode.NONE);
		#end
		trace("I need to be fixed!");
	}
}