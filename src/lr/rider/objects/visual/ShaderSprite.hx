package lr.rider.objects.visual;

import openfl.Vector;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.GraphicsShader;
import openfl.display.Sprite;
import openfl.utils.Assets;

/**
 * ...
 * @author kevansevans
 */
class ShaderSprite extends Sprite
{
	var attached_asset:Sprite;
	var bitmapData:BitmapData;
	var bitmap:Bitmap;
	public var tris:Vector<Float>;
	public var sp_shader:GraphicsShader;
	
	public function new(?_asset:String) 
	{
		super();
		
		if (_asset == null) return;
		
		attached_asset = Assets.getMovieClip(_asset);
		
		bitmapData = new BitmapData(Std.int(attached_asset.width), Std.int(attached_asset.height), true, 0x00FFFFFF);
		bitmapData.draw(attached_asset);
		bitmap = new Bitmap(bitmapData);
		
		#if sys
		sp_shader = new GraphicsShader();
		sp_shader.bitmap.input = bitmapData;
		#end
		
		tris = getTris();
		
		graphics.clear();
		graphics.beginBitmapFill(bitmapData);
		graphics.drawTriangles(tris);
		
		#if flash
		this.cacheAsBitmap = true;
		#end
	}
	function getTris():Vector<Float>
	{
		var f_array:Array<Float> = new Array();
		
		f_array = [0, 0, 
		0, bitmapData.height, 
		bitmapData.width, bitmapData.height,
		0, 0,
		bitmapData.width, 0,
		bitmapData.width, bitmapData.height];
		
		return (new Vector(f_array.length, null, f_array));
	}
}
