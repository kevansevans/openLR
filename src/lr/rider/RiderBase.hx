package lr.rider;

import cpp.Object;
import openfl.display.Sprite;
import openfl.geom.Point;

import global.Common;
import global.SVar;
import global.engine.RiderManager;
import lr.nodes.Grid;
import lr.rider.objects.FlagMarker;
import lr.rider.objects.VisBase;
import lr.rider.objects.StartPointVis;
import lr.rider.objects.visual.B2Bosh;
import lr.rider.phys.skeletons.B2Skeleton;
import lr.rider.phys.frames.B2Frame;
import lr.rider.phys.frames.FrameBase;
import lr.rider.phys.scarf.B2Scarf;
import lr.rider.phys.scarf.ScarfBase;
import lr.rider.phys.SkeletonBase;

/**
 * ...
 * @author Kaelan Evans
 */
enum abstract RiderType(Int) from Int {
	public var Beta1:Int;
	public var Beta2:Int; 
}
enum abstract SubFrame(Int) from Int {
	public var Momentum:Int;
	public var Step1:Int;
	public var Step2:Int;
	public var Step3:Int;
	public var Step4:Int;
	public var Step5:Int;
	public var FullTick:Int;
}
enum abstract Rider(Int) from Int {
	public var Bosh:Int; //0xD20202, 0xFFFFFF
	public var Coco:Int; //0xD977E6, 0xFFCDFF
	public var Fin:Int;  //0x108000, 0xFFFFFF
	public var Essi:Int; //0x65ECFF, 0xFBCCE3
	public var Chaz:Int; //0x1e373b, 0xb3ea44
}
class RiderBase extends Sprite
{
	public var body:FrameBase;
	public var skeleton:SkeletonBase;
	public var clips:VisBase;
	public var scarf:ScarfBase;
	public var start_point:StartPointVis;
	
	public var grav:Point;
	
	public var checkpoint:FlagMarker;
	
	public var bone_vis:Bool = false;
	public var vel_vis:Bool = false;
	public var scarf_vis:Bool = true;
	public var transparency:Float = 1;
	
	public var color_a:Int;
	public var color_b:Int;
	public var rider_alpha:Float = 10;
	public var rider_angle:Float = 0;
	public var rider_name:String;
	public var rider_y_flip:Bool = false;
	public var rider_x_flip:Bool = false;
	public var rider_scale:Float = 0.5;
	public var rider_x_velocity:Float = 0.4;
	public var rider_y_velocity:Float = 0;
	public var rider_pos_x:Float = 0;
	public var rider_pos_y:Float = 0;
	public var spawn:Null<Int>;
	public var despawn:Null<Int>;
	
	public var sim_flags:Object = {
		b_crashed : false,
	}
	public var rider_flags:Object = {
		
	}
	
	var tick_frame = SubFrame.FullTick;
	
	public var riderID:Int;
	
	public function new(_type:Int, _x:Float, _y:Float, _id:Int) 
	{
		super();
		
		this.rider_pos_x = _x;
		this.rider_pos_y = _y;
		
		this.riderID = _id;
		
		this.body = new B2Frame(_x, _y, this.riderID);
		this.skeleton = new B2Skeleton(this.body.anchors, this.riderID);
		this.scarf = new B2Scarf(this.body.anchors[5], this.body.anchors[0], _x, _y, this.riderID);
		this.clips = new B2Bosh(this.body, this.scarf, this.skeleton, this, this.riderID);
		this.addChild(this.clips);
		
		this.grav = new Point(0, 0.175);
		
		this.checkpoint = new FlagMarker(this.riderID);
		this.addChild(this.checkpoint);
		this.checkpoint.visible = false;
	}
	public function set_init_start() {
		this.start_point = new StartPointVis(this.riderID);
		this.addChildAt(this.start_point, 0);
		
		this.start_point.x = this.body.anchors[1].pos.x;
		this.start_point.y = this.body.anchors[1].pos.y;
		
		this.start_point.set_base_properties();
	}
	public function update_color(_a:Int = 0xD20202, _b:Int = 0xFFFFFF) 
	{
		this.start_point.set_color(_a, _b);
		this.clips.set_scarf_color(_a, _b);
		this.checkpoint.set_color(_a, _b);
		this.color_a = _a;
		this.color_b = _b;
	}
	public function update_name(_name:String) {
		this.start_point.set_rider_name(_name);
		this.rider_name = _name;
	}
	public function adjust_rider_dimensions() {
		this.body.set_frame_angle(this.rider_angle);
		this.scarf.set_frame_angle(this.rider_angle);
		this.body.adjust_velocity_start(this.rider_x_velocity, this.rider_y_velocity);
		this.scarf.adjust_velocity_start(this.rider_x_velocity, this.rider_y_velocity);
	}
	public function set_start_angle(_angle:Float) {
		this.rider_angle = _angle;
		this.body.set_frame_angle(_angle);
		this.scarf.set_frame_angle(_angle);
		this.set_start_velocity(this.rider_x_velocity, this.rider_y_velocity);
	}
	public function set_start_velocity(_x:Float, _y:Float) {
		this.rider_x_velocity = _x;
		this.rider_y_velocity = _y;
		this.body.adjust_velocity_start(_x, _y);
		this.scarf.adjust_velocity_start(_x, _y);
	}
	public function set_rider_play_mode() {
		this.start_point.visible = false;
	}
	public function set_rider_edit_mode() {
		this.start_point.visible = true;
	}
	public function step_rider()
	{
		while (tick_frame != SubFrame.FullTick) {
			this.step_rider_sub();
		}
		this.iterate();
		RiderManager.speed[this.riderID] = Math.floor(Math.sqrt(Math.pow(this.body.anchors[5].dir.x, 2) + Math.pow(this.body.anchors[5].dir.y - 0.175, 2)) * 1000) / 1000;
	}
	public function iterate() {
		this.body.verlet(this.grav);
		this.scarf.flutter();
		this.scarf.verlet(this.grav);
		for (a in 0...6) {
			this.skeleton.constrain();
			this.scarf.constrain();
			this.collision();
		}
		this.body.crash_check();
	}
	public function step_rider_loop(_v) {
		while (tick_frame != SubFrame.FullTick) {
			this.step_rider_sub();
		}
		for (a in 0..._v) this.iterate();
		RiderManager.speed[this.riderID] = Math.floor(Math.sqrt(Math.pow(this.body.anchors[5].dir.x, 2) + Math.pow(this.body.anchors[5].dir.y - 0.175, 2)) * 1000) / 1000;
	}
	public function reset() 
	{
		this.set_start(this.rider_pos_x, this.rider_pos_y);
	}
	public function set_start(_x:Float, _y:Float) {
		this.body.set_start(_x, _y);
		this.scarf.set_start(_x, _y);
		this.rider_pos_x = this.body.anchors[0].pos.x;
		this.rider_pos_y = this.body.anchors[0].pos.y;
		this.start_point.x = this.body.anchors[1].pos.x;
		this.start_point.y = this.body.anchors[1].pos.y;
		RiderManager.crash[this.riderID] = false;
	}
	public function step_rider_sub() {
		switch (this.tick_frame) {
			case 0 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.tick_frame = SubFrame.Step1;
			case 1 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.tick_frame = SubFrame.Step2;
			case 2 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.tick_frame = SubFrame.Step3;
			case 3 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.tick_frame = SubFrame.Step4;
			case 4 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.tick_frame = SubFrame.Step5;
			case 5 :
				this.skeleton.constrain();
				this.scarf.constrain();
				this.collision();
				this.body.crash_check();
				this.tick_frame = SubFrame.FullTick;
			case 6 :
				this.body.verlet(this.grav);
				this.scarf.verlet(this.grav);
				this.tick_frame = SubFrame.Momentum;
		}
	}
	public function update_checkpoint(_enabled:Bool, _renew:Bool = true) {
		if (_enabled) {
			this.checkpoint.visible = true;
		} else {
			this.checkpoint.visible = false;
			return;
		}
		if (_renew) {
			this.body.save_position();
			this.checkpoint.update(SVar.flagged_frame);
			this.checkpoint.x = this.body.anchors[4].pos.x;
			this.checkpoint.y = this.body.anchors[4].pos.y;
		}
	}
	function collision() 
	{
		for (dot in this.body.anchors) {
			
			var dotGridPos = Common.gridPos(dot.pos.x, dot.pos.y);
			
			for (x in -1...2) {
				
				var gx = dotGridPos.x + x;
				if (Grid.grid[gx] == null) continue;
				
				for (y in -1...2) {
					
					var gy = dotGridPos.y + y;
					if (Grid.grid[gx][gy] == null) continue;
					
					for (line in Grid.grid[gx][gy].secondary) {
						line.collide(dot);
					}
				}
			}
		}
	}
}