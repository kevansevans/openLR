package lr.rider;

import haxe.ds.Vector;
import openfl.geom.Point;

import global.SVar;
import global.engine.RiderManager;

/**
 * ...
 * @author Kaelan Evans
 */
class RiderRecorder 
{
	var info:Array<Array<RiderTopInfo>>;
	public function new() 
	{
		this.info = new Array();
		this.info[0] = new Array();
	}
	public function index_frame(_frame:Int, _rider:RiderBase) {
		if (this.info[_rider.riderID] == null) {
			this.info[_rider.riderID] = new Array();
		}
		this.info[_rider.riderID][_frame] = {};
		this.info[_rider.riderID][_frame].body_pos = new Vector(_rider.body.anchors.length);
		this.info[_rider.riderID][_frame].scarf_pos = new Vector(_rider.body.anchors.length);
		for (a in 0..._rider.body.anchors.length) {
			this.info[_rider.riderID][_frame].body_pos[a] = {};
			this.info[_rider.riderID][_frame].body_pos[a].pos = _rider.body.anchors[a].pos;
			this.info[_rider.riderID][_frame].body_pos[a].vel = _rider.body.anchors[a].vel;
		}
		for (b in 0..._rider.scarf.anchors.length) {
			this.info[_rider.riderID][_frame].scarf_pos[b] = {};
			this.info[_rider.riderID][_frame].scarf_pos[b].pos = _rider.scarf.anchors[b].pos;
			this.info[_rider.riderID][_frame].scarf_pos[b].vel = _rider.scarf.anchors[b].vel;
		}
		this.info[_rider.riderID][_frame].speed = 0.0;
		this.info[_rider.riderID][_frame].crash = RiderManager.crash[_rider.riderID];
	}
	public function inject_frame(_frame:Int, _rider:RiderBase) { //this doubles as the rewind
		if (this.info[_rider.riderID] == null) {
			return;
		}
		if (this.info[_rider.riderID][_frame] == null) {
			return;
		}
		for (a in 0..._rider.body.anchors.length) {
			_rider.body.anchors[a].pos = this.info[_rider.riderID][_frame].body_pos[a].pos;
			_rider.body.anchors[a].vel = this.info[_rider.riderID][_frame].body_pos[a].vel;
		}
		for (b in 0..._rider.scarf.anchors.length) {
			_rider.scarf.anchors[b].pos = this.info[_rider.riderID][_frame].scarf_pos[b].pos;
			_rider.scarf.anchors[b].vel = this.info[_rider.riderID][_frame].scarf_pos[b].vel;
		}
		//this.info[_rider.riderID][_frame].speed = 0.0;
		RiderManager.crash[_rider.riderID] = this.info[_rider.riderID][_frame].crash;
		SVar.frames = _frame;
	}
}
typedef RiderTopInfo = {
	?body_pos:Vector<PosInfo>,
	?scarf_pos:Vector<PosInfo>,
	?speed:Float,
	?crash:Bool,
}
typedef PosInfo = {
	?pos:Point,
	?vel:Point
}