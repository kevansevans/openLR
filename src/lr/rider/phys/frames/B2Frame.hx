package lr.rider.phys.frames;

import haxe.ds.Vector;

import global.engine.RiderManager;
import lr.rider.phys.anchors.CPoint;

/**
 * ...
 * @author Kaelan Evans
 */
class B2Frame extends FrameBase
{
	public function new(_x:Float, _y:Float, _id:Int) 
	{
		super();
		
		this.riderID = _id;
		
		this.start_x = _x;
		this.start_y = _y;
		
		this.anchors = new Vector(10); //Rider contact points
		this.anchors[0] = new CPoint(0, 0, 0.8, 0); //2nd Peg
		this.anchors[1] = new CPoint(0, 10, 0, 1); //first peg
		this.anchors[2] = new CPoint(30, 10, 0, 2); //Lower Nose
		this.anchors[3] = new CPoint(35, 0, 0, 3); //String
		this.anchors[4] = new CPoint(10, 0, 0.8, 4); //Butt
		this.anchors[5] = new CPoint(10, -11, 0.8, 5); //Shoulder
		this.anchors[6] = new CPoint(23, -10, 0.1, 6); //hand
		this.anchors[7] = new CPoint(23, -10, 0.1, 7); //hand
		this.anchors[8] = new CPoint(20, 10, 0, 8); //Foot
		this.anchors[9] = new CPoint(20, 10, 0, 9); //Foot
		
		for (a in 0...anchors.length) {
			anchors[a].pos.x *= 0.5;
			anchors[a].pos.y *= 0.5;
		}
		for (i in 0...anchors.length) { 
			anchors[i].pos.x = anchors[i].pos.x + this.start_x;
			anchors[i].pos.y = anchors[i].pos.y + this.start_y;
			anchors[i].vel.x = anchors[i].pos.x - 0.4;
			anchors[i].vel.y = anchors[i].pos.y;
		}
	}
	public override function set_start(_x:Float, _y:Float) {
		this.start_x = _x;
		this.start_y = _y;
		this.reset();
	}
	public override function reset() {
		this.anchors[0].pos.x = 0;
		this.anchors[0].pos.y = 0;
		this.anchors[1].pos.x = 0;
		this.anchors[1].pos.y = 10;
		this.anchors[2].pos.x = 30;
		this.anchors[2].pos.y = 10;
		this.anchors[3].pos.x = 35;
		this.anchors[3].pos.y = 0;
		this.anchors[4].pos.x = 10;
		this.anchors[4].pos.y = 0;
		this.anchors[5].pos.x = 10;
		this.anchors[5].pos.y = -11;
		this.anchors[6].pos.x = 23;
		this.anchors[6].pos.y = -10;
		this.anchors[7].pos.x = 23;
		this.anchors[7].pos.y = -10;
		this.anchors[8].pos.x = 20;
		this.anchors[8].pos.y = 10;
		this.anchors[9].pos.x = 20;
		this.anchors[9].pos.y = 10;
		
		for (i in anchors) {
			i.pos.x *= 0.5;
			i.pos.y *= 0.5;
			i.pos.x += this.start_x;
			i.pos.y += this.start_y;
			i.vel.x = i.pos.x - 0.4;
			i.vel.y = i.pos.y;
		}
		
		//this.set_frame_angle();
	}
	override public function crash_check() {
		var _loc4:Float = this.anchors[3].pos.x - this.anchors[0].pos.x;
		var _loc5:Float = this.anchors[3].pos.y - this.anchors[0].pos.y;
		if (_loc4 * (this.anchors[1].pos.y - this.anchors[0].pos.y) - _loc5 * (this.anchors[1].pos.x - this.anchors[0].pos.x) < 0)
		{
			RiderManager.crash[this.riderID] = true; //Tail fakie counter measure.
		}
		if (_loc4 * (this.anchors[5].pos.y - this.anchors[4].pos.y) - _loc5 * (this.anchors[5].pos.x - this.anchors[4].pos.x) > 0)
		{
			RiderManager.crash[this.riderID] = true; //headflip check. Defs more of a bug than tail fake, prevents head from being logged beneath the sled.
		}
	}
}