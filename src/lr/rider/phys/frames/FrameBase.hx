package lr.rider.phys.frames;

import haxe.ds.Vector;
import openfl.geom.Point;

import lr.rider.phys.anchors.CPoint;

/**
 * ...
 * @author Kaelan Evans
 */
class FrameBase 
{
	public var anchors:Vector<CPoint>;

	private var start_x:Float;
	private var start_y:Float;
	
	private var riderID:Int;
	
	private var angle:Float = 0;
	private var velX:Float = 0.4;
	private var velY:Float = 0;
	public function new() 
	{
		
	}
	public function verlet(_gravity:Point)
	{
		for (a in anchors) {
			a.verlet(_gravity);
		}
	}
	public function save_position() {
		for (a in anchors) {
			a.save();
		}
	}
	public function restore_position() {
		for (a in anchors) {
			a.restore();
		}
	}
	public function crash_check()
	{
		
	}
	public function reset() {
		
	}
	public function set_start(_x:Float, _y:Float) {
		
	}
	public function set_frame_angle(_angle:Float = null) {
		
		return;
		
		if (_angle != null) {
			this.angle = _angle;
		}
		var _locRad = angle * (Math.PI / 180);
		for (a in 0...anchors.length) {
			var x1 = anchors[a].pos.x - anchors[0].pos.x;
			var y1 = anchors[a].pos.y - anchors[0].pos.y;
			var x2 = (x1 * Math.cos(_locRad)) - (y1 * Math.sin(_locRad));
			var y2 = (y1 * Math.cos(_locRad)) + (x1 * Math.sin(_locRad));
			anchors[a].pos.x = x2; 
			anchors[a].pos.y = y2; 
		}
		this.adjust_velocity_start();
	}
	public function adjust_velocity_start(_vx:Float = 0.4, _vy:Float = 0) {
		for (i in 0...anchors.length) {
			anchors[i].pos.x = anchors[i].pos.x + this.start_x;
			anchors[i].pos.y = anchors[i].pos.y + this.start_y;
			anchors[i].vel.x = anchors[i].pos.x - _vx;
			anchors[i].vel.y = anchors[i].pos.y - _vy;
		}
	}
}