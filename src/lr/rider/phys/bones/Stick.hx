package lr.rider.phys.bones;
import lr.rider.phys.anchors.CPoint;

/**
 * ...
 * @author ...
 */
class Stick 
{
	public var a:CPoint;
	public var b:CPoint;
	public var rest:Float;
	private var riderID:Int;
	public function new(_a:CPoint, _b:CPoint) 
	{
		this.a = _a;
		this.b = _b;
		var _loc2 = a.pos.x - b.pos.x;
        var _loc3 = a.pos.y - b.pos.y;
        rest = Math.sqrt(_loc2 * _loc2 + _loc3 * _loc3);
	}
	public function constrain():Bool {
		var _loc2:Float = a.pos.x - b.pos.x;
        var _loc3:Float = a.pos.y - b.pos.y;
        var _loc4:Float = Math.sqrt(_loc2 * _loc2 + _loc3 * _loc3);
		var _loc5:Float = 0;
		if (_loc4 != 0) { _loc5 = (_loc4 - this.rest) / _loc4 * 0.5; } //divide by zero catch. Prevents NaN soft lock.
        var _loc6:Float = _loc2 * _loc5;
        var _loc7:Float = _loc3 * _loc5;
        a.pos.x = a.pos.x - _loc6;
        a.pos.y = a.pos.y - _loc7;
        b.pos.x = b.pos.x + _loc6;
        b.pos.y = b.pos.y + _loc7;
		return(false);
	}
}