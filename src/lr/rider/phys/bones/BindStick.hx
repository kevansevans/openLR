package lr.rider.phys.bones;

import global.engine.RiderManager;
import lr.rider.phys.anchors.CPoint;
import lr.rider.phys.bones.Stick;

/**
 * ...
 * @author ...
 */
class BindStick extends Stick
{
	private var endurance:Float;
	public function new(_a:CPoint, _b:CPoint, _id:Int) 
	{
		super(_a, _b);
		this.riderID = _id;
		this.endurance = 0.057 * this.rest * 0.5;
	}
	override public function constrain():Bool {
		var _loc2:Float = a.pos.x - b.pos.x;
        var _loc3:Float = a.pos.y - b.pos.y;
        var _loc4:Float = Math.sqrt(_loc2 * _loc2 + _loc3 * _loc3);
		var _loc5:Float = 0;
		if (_loc4 != 0) { _loc5 = (_loc4 - rest) / _loc4 * 0.5; } //divide by zero catch. Prevents NaN soft lock.
		if (_loc5 >= this.endurance || RiderManager.crash[this.riderID]) {
			RiderManager.crash[this.riderID] = true;
			return(true);
		}
        var _loc6:Float = _loc2 * _loc5;
        var _loc7:Float = _loc3 * _loc5;
        a.pos.x = a.pos.x - _loc6;
        a.pos.y = a.pos.y - _loc7;
        b.pos.x = b.pos.x + _loc6;
        b.pos.y = b.pos.y + _loc7;
		return(false);
	}
}