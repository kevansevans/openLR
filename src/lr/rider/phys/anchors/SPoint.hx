package lr.rider.phys.anchors;
import openfl.geom.Point;

/**
 * ...
 * @author Kaelan Evans
 */
class SPoint extends CPoint
{
	static public var airFric:Float = 0.9;
	
	public function new(_x:Float, _y:Float) 
	{
		super(_x, _y, 0, 0);
	}
	override public function verlet(grav:Point) {
		this.dir = new Point((this.pos.x - this.vel.x) * SPoint.airFric + grav.x, (this.pos.y - this.vel.y) * SPoint.airFric + grav.y);
        this.vel = new Point(this.pos.x, this.pos.y);
        this.pos = new Point(this.vel.x + this.dir.x, this.vel.y + this.dir.y);
	}
}