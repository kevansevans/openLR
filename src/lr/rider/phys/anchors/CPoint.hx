package lr.rider.phys.anchors;
import openfl.geom.Point;

/**
 * ...
 * @author ...
 */
class CPoint 
{
	public var pos:Point;
	public var vel:Point;
	public var dir:Point;
	public var fr:Float;
	public var ID:Int;
	
	public var spos:Point;
	public var svel:Point;
	
	public function new(_x:Float, _y:Float, _fr:Float, _id:Int) 
	{
		this.pos = new Point(_x, _y);
		this.dir = new Point(0, 0);
		this.vel = new Point(0, 0);
		this.fr = _fr;
		this.ID = _id;
	}
	public function verlet(_grav:Point) {
		this.dir = new Point(this.pos.x - this.vel.x + _grav.x, this.pos.y - this.vel.y + _grav.y);
		this.vel = new Point(this.pos.x, this.pos.y);
		this.pos = new Point(this.vel.x + this.dir.x, this.vel.y + this.dir.y);
	}
	public function save() {
		this.spos = this.pos;
		this.svel = this.vel;
	}
	public function restore() {
		this.pos = this.spos;
		this.vel = this.svel;
	}
}