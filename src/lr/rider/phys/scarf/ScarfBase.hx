package lr.rider.phys.scarf;

import haxe.ds.Vector;
import openfl.geom.Point;

import lr.rider.phys.anchors.CPoint;
import lr.rider.phys.bones.ScarfStick;
import lr.rider.phys.anchors.SPoint;

/**
 * ...
 * @author Kaelan Evans
 */
class ScarfBase 
{
	public var anchors:Vector<SPoint>;
	public var edges:Vector<ScarfStick>;
	private var start_x:Float;
	private var start_y:Float;
	private var riderID:Int;
	public var shoulder:CPoint;
	public var origin:CPoint;
	
	public function new() 
	{
		
	}
	public function reset() {
		
	}
	public function constrain() {
		for (a in edges) {
			a.constrain(); //need to figure out how to keep slinky scarf
		}
	}
	public function verlet(_grav:Point)
	{
		for (a in anchors) {
			a.verlet(_grav);
		}
	}
	public function set_start(_x:Float, _y:Float) {
		
	}
	public function set_frame_angle(_angle:Float) {
		if (_angle == 0) {
			return;
		}
		var _locRad = _angle * (Math.PI / 180);
		for (a in 0...anchors.length) {
			var x1 = anchors[a].pos.x - this.origin.pos.x;
			var y1 = anchors[a].pos.y - this.origin.pos.y;
			var x2 = (x1 * Math.cos(_locRad)) - (y1 * Math.sin(_locRad));
			var y2 = (y1 * Math.cos(_locRad)) + (x1 * Math.sin(_locRad));
			anchors[a].pos.x = x2; 
			anchors[a].pos.y = y2; 
		}
		this.adjust_velocity_start();
	}
	public function adjust_velocity_start(_vx:Float = 0.4, _vy:Float = 0) {
		for (i in 0...anchors.length) {
			anchors[i].pos.x = anchors[i].pos.x + this.start_x;
			anchors[i].pos.y = anchors[i].pos.y + this.start_y;
			anchors[i].vel.x = anchors[i].pos.x - _vx;
			anchors[i].vel.y = anchors[i].pos.y - _vy;
		}
	}
	public function flutter() {
		
	}
}