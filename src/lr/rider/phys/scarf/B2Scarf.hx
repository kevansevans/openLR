package lr.rider.phys.scarf;

import haxe.ds.Vector;
import lr.rider.phys.scarf.ScarfBase;
import lr.rider.phys.anchors.CPoint;

import lr.rider.phys.anchors.SPoint;
import lr.rider.phys.bones.ScarfStick;

/**
 * ...
 * @author Kaelan Evans
 */
class B2Scarf extends ScarfBase
{
	public function new(_shoulder:CPoint, _origin:CPoint, _x:Float, _y:Float, _id:Int) 
	{
		super();
		
		this.riderID = _id;
		
		this.start_x = _x;
		this.start_y = _y;
		this.shoulder = _shoulder;
		this.origin = _origin;
		
		this.anchors = new Vector(6); //Scarf contact points
		this.anchors[0] = new SPoint(7, -10);
		this.anchors[1] = new SPoint(3, -10);
		this.anchors[2] = new SPoint(0, -10);
		this.anchors[3] = new SPoint(-4, -10);
		this.anchors[4] = new SPoint(-7, -10);
		this.anchors[5] = new SPoint( -11, -10);
		
		for (a in anchors) {
			a.pos.x *= 0.5;
			a.pos.y *= 0.5;
		}
		for (i in 0...anchors.length) {
			anchors[i].pos.x = anchors[i].pos.x + this.start_x;
			anchors[i].pos.y = anchors[i].pos.y + this.start_y;
			anchors[i].vel.x = anchors[i].pos.x - 0.4;
			anchors[i].vel.y = anchors[i].pos.y;
		}
		
		this.edges = new Vector(6);
		this.edges[0] = new ScarfStick(this.shoulder, this.anchors[0]);
		this.edges[1] = new ScarfStick(this.anchors[0], this.anchors[1]);
		this.edges[2] = new ScarfStick(this.anchors[1], this.anchors[2]);
		this.edges[3] = new ScarfStick(this.anchors[2], this.anchors[3]);
		this.edges[4] = new ScarfStick(this.anchors[3], this.anchors[4]);
		this.edges[5] = new ScarfStick(this.anchors[4], this.anchors[5]);
	}
	override public function reset()
	{
		this.anchors[0].pos.x = 7;
		this.anchors[0].pos.y = -10;
		this.anchors[1].pos.x = 3;
		this.anchors[1].pos.y = -10;
		this.anchors[2].pos.x = 0;
		this.anchors[2].pos.y = -10;
		this.anchors[3].pos.x = -4;
		this.anchors[3].pos.y = -10;
		this.anchors[4].pos.x = -7;
		this.anchors[4].pos.y = -10;
		this.anchors[5].pos.x = -11;
		this.anchors[5].pos.y = -10;
		
		for (a in anchors) {
			a.pos.x *= 0.5;
			a.pos.y *= 0.5;
		}
		for (i in 0...anchors.length) {
			anchors[i].pos.x = anchors[i].pos.x + this.start_x;
			anchors[i].pos.y = anchors[i].pos.y + this.start_y;
			anchors[i].vel.x = anchors[i].pos.x - 0.4;
			anchors[i].vel.y = anchors[i].pos.y;
		}
	}
	public override function set_start(_x:Float, _y:Float) {
		this.start_x = _x;
		this.start_y = _y;
		this.reset();
	}
	override public function flutter() 
	{
		this.anchors[1].pos.x += Math.random() * 0.3 * -Math.min(this.shoulder.dir.x, 125);
		this.anchors[1].pos.y += Math.random() * 0.3 * -Math.min(this.shoulder.dir.y, 125);
	}
}