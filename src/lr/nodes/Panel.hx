package lr.nodes;

import openfl.display.Shape;
import openfl.display.Sprite;
import openfl.geom.Matrix;

import global.Common;
import global.SVar;
import lr.lines.LineBase;

/**
 * ...
 * @author Kaelan Evans
 */
class Panel
{
	public static var _width:Int = SVar.node_tilesize;
	public static var _height:Int = SVar.node_tilesize;
	
	public var primary:Array<LineBase>;
	public var backLayer:Array<Sprite>;
	public var lowFrame = -1;
	public var vismask:Shape;
	
	private var offset_x:Int = 0;
	private var offset_y:Int = 0;
	
	public var panelPosX:Int;
	public var panelPosY:Int;
	public var panelIDName:String;
	
	public var onStage(default, set):Bool = false;
	
	public function new(_x:Int, _y:Int) 
	{
		this.panelPosX = _x;
		this.panelPosY = _y;
		this.offset_y = _y * Panel._height;
		this.offset_x = _x * Panel._width;
		this.panelIDName = "x" + _x + "y" + _y;
		
		this.primary = new Array();
	}
	public function inject_line(_line:LineBase) {
		this.primary.push(_line);
		_line.vis.x = _line.a.x;
		_line.vis.y = _line.a.y;
		_line.vis.cacheAsBitmap = true;
		_line.vis.update(this.onStage, this.panelIDName);
	}
	public function remove_line(_line:LineBase) {
		this.primary.remove(_line);
	}
	private function set_onStage(_v:Bool):Bool {
		for (a in this.primary) {
			a.vis.update(_v, this.panelIDName);
		}
		return onStage = _v;
	}
	public function update() {
		for (a in this.primary) {
			a.vis.render();
		}
	}
}