package lr.nodes;

import openfl.utils.Object;

import global.Common;
import global.CVar;
import global.SVar;
import lr.lines.LineBase;

@:enum abstract Action(Int) from Int to Int {
	public var undo_line:Int = 0;
	public var undo_action:Int = 1;
	public var redo_line:Int = 2;
	public var redo_action:Int = 3;
}

/**
 * ...
 * @author Kaelan Evans
 *
 * Grid is the base line location manager. It handles the caching of each line drawn so they can be easily accesed using nearest neighbor like algorithms. This class is crucial
 * in maintaining compatibility, including backwards, between other Line Rider builds.
 * 
 * It is not recommended modifying this unless you understand what you're doing. Minor changes can result in immediate breaking of vanilla physics.
 * 
 */
class Grid
{
	public var lines:Array<LineBase>; //Primary storage
	public var redo_lines:Array<LineBase>; //If a line gets removed, it gets put here
	
	public static var grid:Map<Int, Map<Int, Storage>>; //2D array for collision checks. Indexed in 14 x 14 squares.
	public static var tile:Map<Int, Map<Int, Panel>>; //2D array for rendering occlusion
	public static var panelList:Array<Panel>; //used for keeping treack of what panels are visible at the moment.
	public static var lowFrame:Int = -1;
	
	public function new()
	{
		this.lines = new Array();
		this.redo_lines = new Array();
		Common.gGrid = this;
		Grid.grid = new Map();
		Grid.tile = new Map();
		Grid.panelList = new Array();
	}
	public function add_remove_action(_type:Int) {
		switch (_type) {
			case Action.undo_line :
				while (true) {
					if (this.lines.length == 0) return;
					var _loc1 = this.lines.pop();
					if (_loc1 == null) {
						continue;
					} else {
						this.remove_line(_loc1);
						break;
					}
				}
			case Action.redo_line :
				this.cacheLine(this.redo_lines.pop());
			case Action.undo_action :
			case Action.redo_action :
		}
	}
	public function new_grid()
	{
		this.lines = new Array();
		this.redo_lines = new Array();
		SVar.lineCount = 0;
		SVar.lineCount_blue = 0;
		SVar.lineCount_red = 0;
		SVar.lineCount_green = 0;
		SVar.lineID = 0;
		Common.gTextInfo.update();
		Grid.grid = new Map();
	}
	public function updateRegistry(_line:LineBase) {
		this.remove_line(_line);
		this.cacheLine(_line);
	}
	public function getLastLine():Null<LineBase> {
		for (a in 0...this.lines.length) {
			if (this.lines[this.lines.length - a] == null) continue;
			return(this.lines[this.lines.length - a]);
		}
		return null;
	}
	public function getFirstLine():Null<LineBase> {
		for (a in 0...this.lines.length) {
			if (this.lines[a] == null) continue;
			return(this.lines[a]);
		}
		return null;
	}
	public function cacheLine(_line:LineBase) {
		if (_line == null) return;
		if (_line.type == LineType.Floor)
		{
			SVar.lineCount_blue += 1;
		}
		else if (_line.type == LineType.Accel)
		{
			SVar.lineCount_red += 1;
		}
		else if (_line.type == LineType.Scene)
		{
			SVar.lineCount_green += 1;
		}
		if (_line.ID == -1) {
			_line.ID = SVar.lineID;
			SVar.lineID += 1;
		} else {
			if (_line.ID > SVar.lineID) SVar.lineID = _line.ID + 1;
		}
		SVar.lineCount += 1;
		this.lines[_line.ID] = _line;
		this.registerInCollisionGrid(_line);
		this.registerInTileGrid(_line);
		Common.gTextInfo.update();
	}
	private function registerInCollisionGrid(line:LineBase) //Function for finding which grid cells a line needs to be indexed into
	{
		//Positions relative to grid
		var gridPointStart:Object = Common.gridPos(line.a.x, line.a.y);
		var gridPointEnd:Object = Common.gridPos(line.b.x, line.b.y);
		
		if (line.d.x == 0 && line.d.y == 0 || gridPointStart.x == gridPointEnd.x && gridPointStart.y == gridPointEnd.y) //Does the line occupy only one cell or posses a length of 0?
		{
			this.register(line, gridPointStart.x, gridPointStart.y); //If yes, register and return.
			return;
			/* Dev note: [line.dx == 0 && line.dy == 0] implies a line with a length of 0. 
			 * I am not entirely sure why a line like this is allowed to be registered,
			 * Beta 2 wouldn't allow a line to be made with a length shorter than 10 units.
			 * It makes this check seem weirdly unecessary with the redundancy of the latter if statement. Maybe it was a safety check?*/
		}
		else
		{
			this.register(line, gridPointStart.x, gridPointStart.y); //If no, register at least once, proceed to register line
		}
		
		//Get the valid range of the line relative to the grid. Needed to account for a line that goes any direction.
		//Note that "Cell" refers to the imaginary box the line gets stored in
		var xcell_a:Int = line.d.x > 0 ? (gridPointEnd.x) : (gridPointStart.x);
		var xcell_b:Int = line.d.x > 0 ? (gridPointStart.x) : (gridPointEnd.x);
		var ycell_a:Int = line.d.y > 0 ? (gridPointEnd.y) : (gridPointStart.y);
		var ycell_b:Int = line.d.y > 0 ? (gridPointStart.y) : (gridPointEnd.y);
		
		//Line traversal loop. Goes down the line and registers accordingly
		var start_x:Float = line.a.x;
		var start_y:Float = line.a.y;
		var invDistX:Float = 1 / line.d.x;
		var invDistY:Float = 1 / line.d.y;
		var difX:Float;
		var difY:Float;
		var tempGridPos = gridPointStart;
		
		while (true)
		{
			//Get next grid values
			//Get next X grid value
			if (gridPointStart.x < 0)
			{
				difX = line.d.x > 0 ? (SVar.node_gridsize + tempGridPos.gx) : ( -SVar.node_gridsize - tempGridPos.gx);
			}
			else
			{
				difX = line.d.x > 0 ? (SVar.node_gridsize - tempGridPos.gx) : (-(tempGridPos.gx + 1));
			}
			//Get next Y grid value
			if (gridPointStart.y < 0)
			{
				difY = line.d.y > 0 ? (SVar.node_gridsize + tempGridPos.gy) : (-SVar.node_gridsize - tempGridPos.gy);
			}
			else
			{
				difY = line.d.y > 0 ? (SVar.node_gridsize - tempGridPos.gy) : (-(tempGridPos.gy + 1));
			}
			//Check which values need to be adjusted according to line slope.
			//Does the line have infinite slope? Then we only need to change the Y value.
			if (line.d.x == 0)
			{
				start_y += difY;
			}
			//Does the line have no slope? Then we only need to change the X value.
			else if (line.d.y == 0)
			{
				start_x += difX;
			}
			else //Line posseses slope, so we need to account for this.
			{
				var _loc6 = start_y + line.d.y * difX * invDistX;
				var _loc7 = start_x + line.d.x * difY * invDistY;
				if (Math.abs(_loc6 - start_y) < Math.abs(difY))
				{
					start_x += difX;
					start_y = _loc6;
				}
				else if (Math.abs(_loc6 - start_y) == Math.abs(difY))
				{
					start_x += difX;
					start_y += difY;
				}
				else
				{
					start_x = _loc7;
					start_y += difY;
				}
			}
			
			//Is the new position still within the range of the line? If yes, register the line and continue the loop. If no, end the loop.
			tempGridPos = Common.gridPos(start_x, start_y);
			if (tempGridPos.x >= xcell_b && tempGridPos.x <= xcell_a && tempGridPos.y >= ycell_b && tempGridPos.y <= ycell_a)
			{
				this.register(line, tempGridPos.x, tempGridPos.y);
				continue;
			} // end if
			return;
		}
	}
	private function register(line:LineBase, _x:Int, _y:Int) 
	{
		if (grid[_x] == null)
		{
			grid[_x] = new Map();
		}
		if (grid[_x][_y] == null)
		{

			grid[_x][_y] = new Storage();
		}
		var a = new Array<Int>();
		a = [_x, _y];
		line.inject_grid_loc(a);
		if (grid[_x][_y].lowFrame != -1) {
			if (Grid.lowFrame == -1) {
				Grid.lowFrame = grid[_x][_y].lowFrame;
			} else if (grid[_x][_y].lowFrame < Grid.lowFrame ) {
				Grid.lowFrame = grid[_x][_y].lowFrame;
			}
		}
		grid[_x][_y].inject_line(line);
	}
	private function registerInTileGrid(line:LineBase) //This function is where the "boundaries" are produced
	{
		var _loc1:Object = Common.tilePos(line.a.x, line.a.y);
		var _loc10:Object = Common.tilePos(line.b.x, line.b.y);
		var _loc13:Int = line.d.x > 0 ? (_loc10.x) : (_loc1.x);
		var _loc11:Int = line.d.x > 0 ? (_loc1.x) : (_loc10.x);
		var _loc7:Int = line.d.y > 0 ? (_loc10.y) : (_loc1.y);
		var _loc12:Int = line.d.y > 0 ? (_loc1.y) : (_loc10.y);
		if (_loc1.x == _loc10.x && _loc1.y == _loc10.y)
		{
			this.register_visual(line, _loc1.x, _loc1.y);
			this.register_visual(line, _loc10.x, _loc10.y);
			return;
		}
		this.register_visual(line, _loc1.x, _loc1.y);
		this.register_visual(line, _loc10.x, _loc10.y);
		var _loc4:Float = line.a.x;
		var _loc3:Float = line.a.y;
		var _loc8:Float = 1 / line.d.x;
		var _loc9:Float = 1 / line.d.y;
		var difX;
		while (true)
		{
			var _loc5;
			if (_loc1.x < 0)
			{
				difX = line.d.x > 0 ? (SVar.node_tilesize + _loc1.gx) : (-SVar.node_tilesize - _loc1.gx);
			}
			else
			{
				difX = line.d.x > 0 ? (SVar.node_tilesize - _loc1.gx) : (-(_loc1.gx + 1));
			}
			if (_loc1.y < 0)
			{
				_loc5 = line.d.y > 0 ? (SVar.node_tilesize + _loc1.gy) : (-SVar.node_tilesize - _loc1.gy);
			}
			else
			{
				_loc5 = line.d.y > 0 ? (SVar.node_tilesize - _loc1.gy) : (-(_loc1.gy + 1));
			}
			if (line.d.x == 0)
			{
				_loc3 = _loc3 + _loc5;
			}
			else if (line.d.y == 0)
			{
				_loc4 = _loc4 + difX;
			}
			else
			{
				var _loc6 = _loc3 + line.d.y * difX * _loc8;
				if (Math.abs(_loc6 - _loc3) < Math.abs(_loc5))
				{
					_loc4 = _loc4 + difX;
					_loc3 = _loc6;
				}
				else if (Math.abs(_loc6 - _loc3) == Math.abs(_loc5))
				{
					_loc4 = _loc4 + difX;
					_loc3 = _loc3 + _loc5;
				}
				else
				{
					_loc4 = _loc4 + line.d.x * _loc5 * _loc9;
					_loc3 = _loc3 + _loc5;
				} // end else if
			} // end else if
			_loc1 = Common.tilePos(_loc4, _loc3);
			if (_loc1.x >= _loc11 && _loc1.x <= _loc13 && _loc1.y >= _loc12 && _loc1.y <= _loc7)
			{
				this.register_visual(line, _loc1.x, _loc1.y);
				continue;
			} // end if
			return;
		}
	}
	private function register_visual(line:LineBase, _x:Int, _y:Int) //This is where the line gets indexed in a 2D array
	{
		if (tile[_x] == null)
		{
			tile[_x] = new Map();
		}
		if (tile[_x][_y] == null)
		{
			tile[_x][_y] = new Panel(_x, _y);
			#if !flash
				tile[_x][_y].onStage = false;
			#end
		}
		var a = new Array<Int>();
		a = [_x, _y];
		line.inject_grid_vis_loc(a);
		tile[_x][_y].inject_line(line);
	}
	public function remove_line(line:LineBase)
	{
		if (line == null) {
			return;
		}
		this.remove_from_grid(line);
		this.lines[line.ID] = null;
		if (line.type == 0)
		{
			--SVar.lineCount_blue;
		}
		else if (line.type == 1)
		{
			--SVar.lineCount_red;
		}
		else if (line.type == 2)
		{
			--SVar.lineCount_green;
		}
		--SVar.lineCount;
		Common.gTextInfo.update();
		this.redo_lines.push(line);
		Common.gTrack.canvas.removeChild(line.vis);
	}
	function remove_from_grid(line:LineBase)
	{
		for (i in 0...line.gridList.length)
		{
			Grid.grid[line.gridList[i][0]][line.gridList[i][1]].remove_line(line);
		}
		for (j in 0...line.gridVisList.length) {
			Grid.tile[line.gridVisList[j][0]][line.gridVisList[j][1]].remove_line(line);
		}
	}
	public function snap(x:Float, y:Float, vert:Int, invert:Bool):Array<Dynamic> //if mouse is close enough to line end when mouse down, line will snap to line
	{
		var _loc2:Float = Math.pow(SVar.snap_distance / Common.gTrack.scaleX, 2);
		var _loc10:Float = x;
		var _loc11:Float = y;
		var _loc17:Bool;
		var _loc18:Bool;
		var _loc6:Float;
		var _loc7:Float;
		var _loc9:Dynamic = null;
		var _loc15 = Common.gridPos(x, y);
		var _loc8:LineBase = null;
		for (_loc14 in -1...2)
		{
			var _loc4 = (_loc15.x + _loc14);
			if (grid[_loc4] == null)
			{
				continue;
			} // end if
			for (_loc5 in -1...2)
			{
				var _loc3 = (_loc15.y + _loc5);
				if (grid[_loc4][_loc3] == null)
				{
					continue;
				} // end if
				for (_loc16 in 0...grid[_loc4][_loc3].secondary.length)
				{
					if (grid[_loc4][_loc3].secondary[_loc16] == null) {continue;}
					var _loc1:LineBase = grid[_loc4][_loc3].secondary[_loc16];
					_loc6 = Math.pow(x - _loc1.a.x, 2) + Math.pow(y - _loc1.a.y, 2);
					_loc7 = Math.pow(x - _loc1.b.x, 2) + Math.pow(y - _loc1.b.y, 2);
					if (_loc6 < _loc2)
					{
						_loc2 = _loc6;
						_loc10 = _loc1.a.x;
						_loc11 = _loc1.a.y;
						_loc9 = 1;
						_loc8 = _loc1;
					} // end if
					if (_loc7 < _loc2)
					{
						_loc2 = _loc7;
						_loc10 = _loc1.b.x;
						_loc11 = _loc1.b.y;
						_loc9 = 2;
						_loc8 = _loc1;
					} //end if
				} // end of for...in
			} // end of for
		} // end of for
		if (_loc9 != null && _loc8 != null)
		{
			_loc17 = vert == _loc9;
			_loc18 = invert == _loc8.inv;
		}
		else {
			_loc17 = true;
			_loc18 = false;
		}
		if (!(_loc17 && !_loc18 || !_loc17 && _loc18))
		{
			_loc9 = false;
			_loc10 = x;
			_loc11 = y;
		}
		else {
			_loc9 = true;
		}
		if (!CVar.local.line_snap)
		{
			_loc9 = false;
		}
		var _locArray:Array<Dynamic> = new Array();
		_locArray = [_loc10, _loc11, _loc9];
		return (_locArray);
	} // End of the function
	public function updateRender(_con:String) {
		switch (_con) {
			case ("Play") :
				for (a in Grid.panelList) {
					trace("Set Rendermode here!");
				}
			case ("Edit") :
				for (a in Grid.panelList) {
					trace("Set Rendermode here!");
				}
		}
	}
}