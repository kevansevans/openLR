package lr.tool.editing;

import openfl.Lib;
import openfl.events.MouseEvent;
import openfl.geom.Point;

import lr.lines.LineBase;
import global.Common;
import global.SVar;
import global.CVar;

/**
 * ...
 * @author Kaelan Evans
 */
class ToolPencil extends ToolAction
{
	private var a:Point;
	private var b:Point;
	private var c:Point;
	private var d:Point;
	public function new() 
	{
		super();
	}
	override public function leftMouseDown(e:MouseEvent) {
		this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(this.a.x, this.a.y, 1, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			this.a = new Point(_locSnapCheck[0], _locSnapCheck[1]);
		}
		this.c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY); 
		this.d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY); 
		this.leftMouseIsDown = true;
	}
	override public function leftMouseMove(e:MouseEvent) 
	{
		if (!this.leftMouseIsDown) return;
		Common.gToolbar.visible = false;
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		Common.gTrack.renderPreview(new LinePreview(this.a, this.b, CVar.mod_shift, SVar.line_type));
		if (Common.get_distance(c, d) >= SVar.line_minLength) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.a, this.b, CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
			this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
			this.c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		}
	}
	override public function leftMouseUp(e:MouseEvent) {
		if (SVar.line_type == 2) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.a, this.b, CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
		}
		Common.gToolbar.visible = true;
		this.leftMouseIsDown = false;
		Common.gTrack.clear_preview();
	}
	override public function rightMouseDown(e:MouseEvent) {
		this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(this.a.x, this.a.y, 1, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			this.a = new Point(_locSnapCheck[0], _locSnapCheck[1]);
		}
		this.c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		this.rightMouseIsDown = true;
	}
	override public function rightMouseMove(e:MouseEvent) 
	{
		if (!this.rightMouseIsDown) return;
		Common.gToolbar.visible = false;
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		this.d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		Common.gTrack.renderPreview(new LinePreview(this.b, this.a, !CVar.mod_shift, SVar.line_type));
		if (Common.get_distance(this.c, this.d) >= SVar.line_minLength) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.b, this.a, !CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
			this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
			this.c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		}
	}
	override public function rightMouseUp(e:MouseEvent) {
		if (SVar.line_type == 2) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.a, this.b, CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
		}
		Common.gToolbar.visible = true;
		Common.gTimeline.visible = true;
		this.rightMouseIsDown = false;
		Common.gTrack.clear_preview();
	}
}