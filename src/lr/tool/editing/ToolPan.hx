package lr.tool.editing;

import openfl.events.MouseEvent;
import openfl.Lib;

import global.Common;

/**
 * ...
 * @author ...
 */
class ToolPan extends ToolAction
{

	public function new() 
	{
		super();
	}
	override function leftMouseDown(e:MouseEvent):Void 
	{
		Common.gTrack.startDrag();
		Lib.current.addEventListener(MouseEvent.MOUSE_MOVE, this.update);
		
	}
	override function leftMouseUp(event:MouseEvent):Void 
	{
		Common.gTrack.stopDrag();
		Lib.current.removeEventListener(MouseEvent.MOUSE_MOVE, this.update);
	}
	function update(e:MouseEvent):Void 
	{
		
	}
}