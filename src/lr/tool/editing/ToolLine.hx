package lr.tool.editing;

import openfl.Lib;
import openfl.events.MouseEvent;
import openfl.geom.Point;

import global.Common;
import global.CVar;
import global.SVar;
import lr.lines.LineBase;

/**
 * ...
 * @author Kaelan Evans
 */
class ToolLine extends ToolAction
{
	private var a:Point;
	private var b:Point;
	private var c:Point;
	private var d:Point;
	private var valid:Bool = false;
	public function new() 
	{
		super();
	}
	override public function leftMouseDown(e:MouseEvent) {
		this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(a.x, a.y, 1, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			a = new Point( _locSnapCheck[0], _locSnapCheck[1]);
		}
		this.c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY); 
		this.d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY); 
		this.leftMouseIsDown = true;
	}
	
	override public function leftMouseMove(event:MouseEvent) 
	{
		if (!this.leftMouseIsDown) return;
		Common.gToolbar.visible = false;
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		this.d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY); 
		if (CVar.mod_x || CVar.local.angle_snap) {
			var _locSnap = this.angle_snap(this.a, this.b);
			b = new Point(_locSnap[0], _locSnap[1]);
		}
		this.valid = true;
		Common.gTrack.renderPreview(new LinePreview(this.a, this.b, CVar.mod_shift, SVar.line_type));
	}
	override public function leftMouseUp(e:MouseEvent) {
		this.leftMouseIsDown = false;
		Common.gTrack.clear_preview();
		Common.gToolbar.visible = true;
		if (!valid) {
			return;
		}
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(b.x, b.y, 2, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			b = new Point(_locSnapCheck[0], _locSnapCheck[1]);
		}
		if (Point.distance(c, d) >= 1) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.a, this.b, CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
		}
		this.valid = false;
	}
	override public function rightMouseDown(e:MouseEvent) {
		this.rightMouseIsDown = true;
		this.a = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(a.x, b.y, 1, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			a = new Point( _locSnapCheck[0], _locSnapCheck[1]);
		}
		c = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
	}
	
	override public function rightMouseMove(event:MouseEvent) 
	{
		if (!this.rightMouseIsDown) return;
		Common.gToolbar.visible = false;
		this.b = new Point(Common.gTrack.mouseX, Common.gTrack.mouseY);
		if (CVar.mod_x || CVar.local.angle_snap) {
			var _locSnap = this.angle_snap(this.a, this.b);
			b = new Point(_locSnap[0], _locSnap[1]);
		}
		d = new Point(Lib.current.stage.mouseX, Lib.current.stage.mouseY);
		Common.gTrack.renderPreview(new LinePreview(this.b, this.a, !CVar.mod_shift, SVar.line_type));
	}
	override public function rightMouseUp(e:MouseEvent) {
		Common.gToolbar.visible = true;
		var _locSnapCheck:Array<Dynamic> = Common.gGrid.snap(this.a.x, this.a.y, 2, CVar.mod_shift);
		if (_locSnapCheck[2] == true && SVar.line_type != 2) {
			b = new Point(_locSnapCheck[0], _locSnapCheck[1]);
		}
		if (Common.get_distance(c, d) >= 1) {
			var _loc1:LineBase = new LineBase(SVar.line_type, this.b, this.a, !CVar.mod_shift);
			Common.gGrid.cacheLine(_loc1);
		}
		Common.gTrack.clear_preview();
		this.rightMouseIsDown = false;
	}
}