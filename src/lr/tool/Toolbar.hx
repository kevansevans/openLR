package lr.tool;

import openfl.Lib;
import openfl.display.Sprite;
import openfl.events.MouseEvent;

import global.Common;
import global.SVar;
import lr.menus.FileMenu;
import lr.menus.SettingsMenu;
import lr.tool.ToolBase;
import components.IconBar;
import components.IconButton;

/**
 * ...
 * @author Kaelan Evans
 * 
 * Tool bar seen across top of screen
 * 
 */
class Toolbar extends Sprite
{

	public static var tool:ToolBase;
	public static var icon:IconButton;
	public static var swatch:IconButton;
	private var pencil:IconButton;
	private var line:IconButton;
	private var eraser:IconButton;
	private var pan:IconButton;
	private var zoom:IconButton;
	private var save:IconButton;
	private var settings:IconButton;
	private var settings_menu:SettingsMenu;
	private var load_menu:FileMenu;
	private var swBlue:IconButton;
	private var swRed:IconButton;
	private var swGreen:IconButton;
	
	public var bar_editor:IconBar;
	public var bar_swatches:IconBar;
	
	private var last_line_type:Null<Int>;
	
	public function new() 
	{
		super();
		
		Common.gToolbar = this;
		
		this.mouseEnabled = false;
		
		Toolbar.tool = new ToolBase();
		Toolbar.tool.set_listeners();
		
		this.bar_editor = new IconBar();
		
		pencil = new IconButton(Icon.pencil);
		pencil.func_up = function(e:MouseEvent) {
			Toolbar.tool.set_tool(ToolType.Pencil);
		}
		this.bar_editor.add_icon(this.pencil);
		
		line = new IconButton(Icon.line);
		line.func_up = function(e:MouseEvent) {
			Toolbar.tool.set_tool(ToolType.Line);
		}
		this.bar_editor.add_icon(this.line);
		
		eraser = new IconButton(Icon.eraser);
		eraser.func_up = function(e:MouseEvent) {
			Toolbar.tool.set_tool(ToolType.Eraser);
			Toolbar.swatch.deselect();
		}
		this.bar_editor.add_icon(this.eraser);
		
		zoom = new IconButton(Icon.zoom);
		zoom.func_up = function(e:MouseEvent) {
			Toolbar.tool.set_tool(ToolType.Zoom);
		}
		this.bar_editor.add_icon(this.zoom);
		
		pan = new IconButton(Icon.pan);
		pan.func_up = function(e:MouseEvent) {
			Toolbar.tool.set_tool(ToolType.Pan);
		}
		this.bar_editor.add_icon(this.pan);
		
		this.bar_swatches = new IconBar();
		
		this.swBlue = new IconButton(Icon.swBlue);
		this.swBlue.func_up = function(e:MouseEvent) {
			SVar.line_type = 0;
			Toolbar.swatch.deselect();
			Toolbar.swatch = this.swBlue;
			this.swBlue.select();
		}
		this.bar_swatches.add_icon(this.swBlue);
		
		this.swRed = new IconButton(Icon.swRed);
		this.swRed.func_up = function(e:MouseEvent) {
			SVar.line_type = 1;
			Toolbar.swatch.deselect();
			Toolbar.swatch = this.swRed;
			this.swRed.select();
		}
		this.bar_swatches.add_icon(this.swRed);
		
		this.swGreen = new IconButton(Icon.swGreen);
		this.swGreen.func_up = function(e:MouseEvent) {
			SVar.line_type = 2;
			Toolbar.swatch.deselect();
			Toolbar.swatch = this.swGreen;
			this.swGreen.select();
		}
		this.bar_swatches.add_icon(this.swGreen);
		
		settings = new IconButton(Icon.settings);
		this.settings_menu = new SettingsMenu();
		this.settings_menu.visible = false;
		this.settings.func_up = this.settings_menu.window.negative.func_up = function(e:MouseEvent):Void {
			this.settings_menu.visible = this.settings_menu.visible == false ? true : false;
			this.settings_menu.x = this.settings_menu.y = 20;
		}
		this.bar_editor.add_icon(settings);
		Lib.current.stage.addChild(this.settings_menu);
		
		save = new IconButton(Icon.file);
		this.load_menu = new FileMenu();
		this.load_menu.visible = false;
		this.save.func_up = this.load_menu.window.negative.func_up = function(e:MouseEvent):Void {
			this.load_menu.visible = this.load_menu.visible == false ? true : false;
			this.load_menu.x = this.load_menu.y = 20;
			this.load_menu.refresh();
		}
		this.bar_editor.add_icon(save);
		Lib.current.stage.addChild(this.load_menu);
		
		for (a in this.bar_editor.iconList) {
			a.func_over = function(e:MouseEvent) {
				Common.gToolBase.set_tool("None");
			}
			a.func_out = function(e:MouseEvent) {
				Common.gToolBase.set_tool(ToolBase.lastTool);
			}
		}
		
		for (b in this.bar_swatches.iconList) {
			b.func_over = function(e:MouseEvent) {
				Common.gToolBase.set_tool("None");
			}
			b.func_out = function(e:MouseEvent) {
				Common.gToolBase.set_tool(ToolBase.lastTool);
			}
		}
		
		Toolbar.icon = pencil;
		icon.select();
		Toolbar.swatch = swBlue;
		swBlue.select();
		
		Toolbar.tool.set_tool(ToolType.Pencil);
		
		this.addChild(this.bar_editor);
		this.bar_editor.update();
		this.bar_editor.x -= this.bar_editor.width / 2;
		
		this.addChild(this.bar_swatches);
		this.bar_swatches.update();
		this.bar_swatches.x = this.bar_editor.x;
		this.bar_swatches.y = 32;
	}
	public function update_icons(_type:String) {
		Toolbar.icon.deselect();
		switch (_type) {
			case ToolType.Pencil :
				Toolbar.icon = this.pencil;
				Toolbar.swatch.select();
			case ToolType.Line :
				Toolbar.icon = this.line;
				Toolbar.swatch.select();
			case ToolType.Eraser :
				Toolbar.icon = this.eraser;
				Toolbar.swatch.deselect();
				SVar.line_type = -1;
			case ToolType.Pan :
				Toolbar.icon = this.pan;
				Toolbar.swatch.deselect();
				this.last_line_type = SVar.line_type;
				SVar.line_type = -1;
			case ToolType.Zoom :
				Toolbar.icon = this.zoom;
				Toolbar.swatch.deselect();
				this.last_line_type = SVar.line_type;
				SVar.line_type = -1;
		}
		Toolbar.icon.select();
	}
	public function update_swatch(_type:Int) {
		swatch.deselect();
		var type = _type;
		if (ToolBase.lastTool == ToolType.Pan || ToolBase.lastTool == ToolType.Zoom) type = this.last_line_type;
		switch(type) {
			case -1:
				Toolbar.swatch.deselect();
			case 0:
				SVar.line_type = 0;
				swatch = swBlue;
			case 1 :
				SVar.line_type = 1;
				swatch = swRed;
			case 2 :
				SVar.line_type = 2;
				swatch = swGreen;
			default :
				SVar.line_type = 0;
				swatch = swBlue;
		}
		swatch.select();
	}
}